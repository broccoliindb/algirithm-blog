---
title: "Binary Search"
displayTitle: "이진탐색"
date: 2020-09-05T00:24:39+09:00
isCJKLanguage: true
draft: false
categories: [search]
levels: []
description: "최소값과 최대값의 평균값으로 추측범위를 좁히는 탐색방법"
tags: ["binary search"]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "6474250cf1910197449bab8d3cba5787"
disqus_title: "Binary Search"
disqus_url: "6474250cf1910197449bab8d3cba5787"
---

## 이진 탐색

이진탐색은 탐색범위를 반씩 줄여서 검색하는 탐색 방법이다.

```
T(N) = O(log2(N))
```

관련 시간복잡도가 어떻게 log2(N)인지는 [머지정렬](/algorithms/merge-sort) 에 설명되어있습니다.

### Pseudo Code

{{< highlight "js" "linenos=table,linenostart=1" >}}

1. min = 1, max = n

2. mid = Math.floor((min + max) / 2)

3. 탐색

4. mid가 타겟일 경우 종료

5. mid가 타겟보다 작을 경우 min = ++mid 후에 2, 3 반복 (min < max 일때)

6. mid가 타겟보다 클 경우 max = --mid 후에 2, 3 반복 (min < max 일때 )

7. min >= max보다 큰 경우 탐색 종료. 1을 더하거나 빼기 이전 mid 값 반환

{{< /highlight >}}



