---
title: "Eratosthenes Sieve"
displayTitle: "에라토스의 체"
date: 2020-10-11T23:49:31+09:00
isCJKLanguage: true
draft: false
categories: [algorithms]
levels: []
description: "소수리스트를 구하는 가장 좋은 방법"
tags: [prime]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "ce6cfe3d2fae22069fa9667c0e6b3a1e"
disqus_title: "Eratosthenes Sieve"
disqus_url: "ce6cfe3d2fae22069fa9667c0e6b3a1e"
---

## 에라토스의 체

원 표현은 SIEVE OF ERATOSTEHNES 라고 한다.
골드바흐의 추측을 풀고 다른 사람의 방식을 체크하다가 알게된 소수리스트를 구하는 방법이다.

ℹ️ 참고영상
{{< youtube V08g_lkKj6Q >}}

### 요약설명 및 소수구하기 방법#1

에라토스의 체는 간단히 설명하면 해당 리스트의 수들 중 소수로 선택된 가장 작은 값들의 배수들을 제거하고 남은 수가 소수다라고 판단하는 방식이다. 아래 소스를 통해 확인해보자

{{< highlight "js" "linenos=table,hl_lines=1,linenostart=1" >}}
const maxnum = 100
const primeList = Array(maxnum).fill(true)
// 0, 1은 소수가 당연히 아님
primeList[0] = false
primeList[1] = false
for (let i = 2; i <= maxnum; i++) {
  // 2의 배수, 3의 배수....n의 배수은 모두 소수가 아님
  for (let j = i*2; j <=maxnum; j = j + i) {
    if(primeList[j]) {
      primeList[j] = false
    }
  }
}

// 아래 키들이 소수값이다.
for (let [key, value] of primeList.entries()) {
    if(value) console.log(key)
}
{{< /highlight >}}

### 기타 소수판별법#2,3

소수는 1보다 큰 양의 정수 중 1과 자기자신만을 가지고 나누어 떨어지는 수를 가리킨다. 따라서 일반적으로는 아래와 같은 방법으로 구한다.

#### 방법#2

{{< highlight "js" "linenos=table,linenostart=1" >}}
const maxnum = 100

let primes = []
for (let i = 2; i <= maxnum; i++) {
  let isPrime = true
  for (let j = 2; j < i; j++) {
    if(i % j === 0) isPrime = false
  }
  if(isPrime) primes.push(i)
}
{{< /highlight >}}

#### 방법#3

방법3은 방법2와 동일하지만 두번째 루프에서 조건을 좀 더 제한한다. 왜냐하면 소수가 아니다라는 의미는 약수가 1과 본인 이외에 더 있다는 의미인데, 약수의 곱을 보면 절반 이후 대칭값을 이루기 때문에 굳이 확인할필요가 없기 때문이다. 약수의 절반값은 대략 타겟의 루트를 씌운값이다.

{{< highlight "js" "linenos=table,linenostart=1" >}}
const maxnum = 100

let primes = []
for (let i = 2; i <= maxnum; i++) {
  let isPrime = true
  for (let j = 2; j * j <= i; j++) {
    if(i % j === 0) isPrime = false
  }
  if(isPrime) primes.push(i)
}

{{< /highlight >}}