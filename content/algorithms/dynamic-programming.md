---
title: "Dynamic Programming"
displayTitle: "다이나믹 프로그래밍"
date: 2020-10-30T16:57:24+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "다이나믹 프로그래밍의 대한 정리"
tags: [dynamic, memoization]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "b0785a2fe6f2049886416113e9b50da7"
disqus_title: "Dynamic Programming"
disqus_url: "b0785a2fe6f2049886416113e9b50da7"
---

## Dynamic Programming

다이나믹 프로그램은 이렇게 표현할 수 있다.

> ***Those who cannot remember the past are condemned to repeat it***

과거를 기억하지 못하면 그것을 반복한다.

그렇다면 과거를 기억하면 반복할 필요가 없다 따라서 `Memoization` 이라고 생각할 수 있다. 대표적으로 피보나치수열이 대표적일 수 있는데 이와 같이 

```
T(N) = T(N-1) + T(N-2)
```

이런식의 상황에서 Memoization을 사용한다면 우리는 재귀를 최소한으로 사용함으로써 stack overflow가 날수있는 현상을 줄일 수 있고, 좀 더 효율적으로 코드가 작성될 수 있다.
