---
title: "Selection Sort"
displayTitle: "선택 정렬"
date: 2020-08-13T14:19:59+09:00
draft: false
isCJKLanguage: true
categories: []
levels: []
description: "선택정렬은 배열의 값중 가장 작은것을 맨 앞으로 옮긴다.

다시 남은것들중에 작은것을 다시 그다음에 갖다 놓는다."
tags: [algorithm, sort]
solutions:  []
authors: ["broccoliindb"]
weight: 0
disqus_identifier: "b9c4477337e7744c9b00c5eadf542a99"
disqus_title: "Selection Sort"
disqus_url: "b9c4477337e7744c9b00c5eadf542a99"
---

## 선택정렬

선택정렬은 배열의 값중 가장 작은것을 맨 앞으로 옮긴다.

다시 남은것들중에 작은것을 다시 그다음에 갖다 놓는다.

따라서 여러번 반복이 되므로 성능 안좋음. N^2

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}
function selectSort(arr) {
  let min, temp;
  for (let i = 0; i < arr.length - 1; i++) {
    for (j = 1 + i; j < arr.length; j++) {
      min = arr[i];
      if (min > arr[j]) {
        temp = min;
        min = arr[j];
        arr[j] = temp;
      }
      arr[i] = min;
    }
  }
  return arr;
}
var arr = [5, 6, 1, 3, 4, 2];
selectSort(arr);
{{< /highlight >}}