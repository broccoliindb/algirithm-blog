---
title: "Merge Sort"
displayTitle: "병합 / 머지 정렬"
date: 2020-08-13T14:23:01+09:00
draft: false
isCJKLanguage: true
categories: [sort]
levels: []
description: "1개 배열을 최종 1개의 요소들로 쪼갠 후 다시 머지하면서 정렬하는 방법"
tags: [algorithm, merge-sort]
solutions: []
authors: ["broccoliindb"]
weight: 0
disqus_identifier: "c32810287884a2cde91ecf32682b5016"
disqus_title: "Merge Sort"
disqus_url: "c32810287884a2cde91ecf32682b5016"
---

## 머지정렬

1개 배열을 최종 1개의 요소들로 쪼갠 후 다시 머지하면서 정렬하는 방법 

ℹ️ [머지정렬분석 참고링크 : 칸아카데미](https://www.khanacademy.org/computing/computer-science/algorithms/merge-sort/a/analysis-of-merge-sort) 

![mergeSortTree](/algorithms/images/mergesortTree.png)

만약 `n = 8` 일때 위의 총 소요 시간은 `3CN`이 된다. 

N = 8일 때, 3 = log2(8) 이것이 `3`CN의 3에 해당 

따라서 머지정렬시 분할에 해당하는 시간 복잡도는

```
 Nlog2(N)
```
 이다. {{< show >}}추가정보{{< /show >}}

{{< hide >}}

### pseudo code

```
 @A {Array}: array,
 @p {Number} : n - r,
 @r {Number} 
 
 if p < r
   q = [(p + r) / 2]
   MergeSort(A, p, q)
   MergeSort(A, q + 1, r)
   Merge(A, p, q, r)

따라서 

T(N) = T(N/2) + T(N/2) + T(Merge arrays of Size N)

     = 2T(N/2) + T(Merge arrays of Size N) 
     
     = 2T(N/2) + O(N) = Nlog2(N)이다.
```

ℹ️ T(Merge arrays of Size N) = O(N) 인 이유는 아래 영상을 참조해주세요
{{< /hide >}}

#### 참고영상

#1 merge sort
{{< youtube 279cymdrmdg >}}

#2 merge sort
{{< youtube C4JjXc0htp0 >}}

#### Merge Sort 구현

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}
function mergeSort(arr) {
  let resultArr = [];
  if (arr.length > 1) {
    const first = mergeSort(arr.splice(0, Math.floor(arr.length / 2)));
    const second = mergeSort(arr);
    while (first.length || second.length) {
      if (first.length && second.length) {
        resultArr.push(first[0] >= second[0] ? second.shift() : first.shift());
      } else {
        if (first.length) {
          resultArr.push(first.pop());
        } else {
          resultArr.push(second.pop());
        }
      }
    }
  } else {
    resultArr.push(arr.pop());
  }
  return resultArr;
}

var arr = [5, 1, 3, 4, 2];
console.log(arr);
mergeSort(arr);

{{< /highlight >}}