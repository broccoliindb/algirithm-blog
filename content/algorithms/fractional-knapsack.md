---
title: "Fractional Knapsack"
displayTitle: "Fractional Knapsack Problem"
date: 2020-11-08T23:32:32+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "무게와 가치가 든 knapsack들이 N개 주어지고 도둑은 그것들을 취할 수 있는 knapsack이 한개 있는데, 이 도둑이 가진 knapsack은 무게 수용치가 정해져 있고 취할 수 있는 knapsack은 분할이 가능하다. 도둑이 최대로 취할 수 있는 방법은?"
tags: [greedy, 탐욕, algorithm]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "b2a22e9405152e9714834cd37806dc12"
disqus_title: "Fractional Knapsack"
disqus_url: "b2a22e9405152e9714834cd37806dc12"
---

## Fractional Knapsack Problem

### 참고영상

{{< youtube m1p-eWxrt6g>}}

무게와 가치가 든 knapsack들이 N개 주어지고 도둑은 그것들을 취할 수 있는 knapsack이 한개 있는데, 이 도둑이 가진 knapsack은 무게 수용치가 정해져 있고 취할 수 있는 knapsack은 분할이 가능하다. 도둑이 최대로 취할 수 있는 방법은?

### 해결 접근 방법

![fractional-knapsack](/algorithms/images/fractional-knapsack.png)

- 각 knapsack의 무게의 따른 가치를 계산한다.
- 단위크기당 가치가 큰 knapsack 순으로 sort한다.
- 수용한계치까지 knapsack을 취한다.

#### [참조 : 탐욕알고리즘링크](/algorithms/greedy)