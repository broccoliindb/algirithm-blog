---
title: "Union Find"
displayTitle: "Union Find"
date: 2020-11-20T10:29:10+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "Union Find(Disjoint Set)은 노드들이 아래 그림과 같이 하나의 부분 집합으로 이루어져있는지 여부를 판단할 때 사용한다."
tags: ["union set", "disjoint set"]
solutions: []
authors: []
weight: 0
disqus_identifier: "c22cd37fcd5dd1d335290ff4447ce281"
disqus_title: "Union Find"
disqus_url: "c22cd37fcd5dd1d335290ff4447ce281"
---
## Union Find

ℹ️ [union find 참고링크: geeksforgeeks](https://www.geeksforgeeks.org/union-find/)

Union Find(Disjoint Set)은 노드들이 아래 그림과 같이 하나의 부분 집합으로 이루어져있는지 여부를 판단할 때 사용한다.

따라서 Union Find의 자료구조는 find, union의 기능을 가지고 있어야하며 초기화 할때 각 노드들은 스스로를 다른 노드들과 배타적인 집합으로 생성해준다.

### Find

특정 노드가 어떤 집합안에 있는지 여부를 판단해준다.
### Union

두개의 부분집합을 하나의 집합으로 합쳐준다.

### 참고소스

Union find를 그래프로 구현해보자.

```js
class DisjointSet {
  constructor(n) {
    this.setList = new Map()
    for (let i = 1; i <= n; i++) {
      this.setList.set(i, this.makeSet())
    }
  }

  makeSet() {
    const singleton = {
      rank: 0
    }
    singleton.parent = singleton
    return singleton
  }

  union(node1, node2) {
    const root1 = this.find(node1)
    const root2 = this.find(node2)
    if (root1 !== root2) {
      if (root1.rank < root2.rank) {
        root1.parent = root2
      } else {
        root2.parent = root1
        if (root1.rank === root2.rank) {
          root1.rank += 1
        }
      }
    }
  }

  find(node) {
    if (node === node.parent) {
      return node
    } else {
      node.parent = this.find(node.parent)
      return node.parent
    }
  }
}
```
