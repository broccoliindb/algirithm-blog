---
title: "Floyd Warshall"
displayTitle: "플로이드 와샬"
date: 2020-10-15T16:43:01+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "모든 정점에서 모든 정점으로의 최단경로를 구하는 알고리즘"
tags: [algorithm, floyd-warshall]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "42a95ef53456f9dba84933a269bcbebc"
disqus_title: "Floyd Warshall"
disqus_url: "42a95ef53456f9dba84933a269bcbebc"
---
## Floyd Warshall

플로이드 와샬은 모든 정점에서 모든 정점으로의 최단 경로를 구하는 알고리즘이다.

### 알고리즘

{{< highlight "js" "linenos=table,linenostart=1" >}}
// k: 중간경로
// i: 시작위치
// j: 종료위치
// N: 노드의 수

for (let k = 0; k < N; k++) {
  for (let i = 0; i < N; i++ ) {
    for (let j = 0; j < N; j++) {
      //조건은 문제가 무엇이냐에 따라 달라짐
      if (matrix[i][k] + matrix[k][j] < matrix[i][j]) {
        matrix[i][j] = matrix[i][k] + matrix[k][j]
      }
    }
  }
}
{{< /highlight >}}

아래그림에서 처럼 노드가 1,2,3,4로 주어질때 4 * 4의 이차행렬의 배열을 생성해준다 

2차배열을 처음 생성할 때, `self->self : 0을 넣어준다`. `나머지는 모두 Infinity를 넣어주는게 좋다`.

이후 위에 알고리즘에 따라 해당 matrix의 값들이 변한다.

변경된 matrix의 값들은 각 노드와 다른 각 노드들과의 최단거리의 값을 의미한다. (조건에 따라 값의 의미는 달라질 수 있음)

![floyd-warshall](/algorithms/images/floyd-warshall.png)

### 관련영상

{{< youtube 4OQeCuLYj-4>}}