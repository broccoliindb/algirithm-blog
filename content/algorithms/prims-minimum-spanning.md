---
title: "Prims Minimum Spanning Tree"
displayTitle: "Prim's Minimum Spanning Tree"
date: 2020-11-08T23:33:00+09:00
isCJKLanguage: true
draft: true
categories: []
levels: []
description: ""
tags: []
solutions: []
authors: []
weight: 0
disqus_identifier: "423b6d6177701e77b271f829758b13ef"
disqus_title: "Prims Minimum Spanning"
disqus_url: "423b6d6177701e77b271f829758b13ef"
---

## Prim's Minimum Spanning Tree

### 참고영상

{{< youtube eB61LXLZVqs>}}

### 해결 접근 방법

#### [참조 : 탐욕알고리즘링크](/algorithms/greedy)

https://www.weeklyps.com/entry/%ED%94%84%EB%A6%BC-%EC%95%8C%EA%B3%A0%EB%A6%AC%EC%A6%98-Prims-algorithm