---
title: "Complex"
displayTitle: "복잡도"
date: 2020-08-13T16:26:00+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "시간복잡도와 공간복잡도의 대한 문서"
tags: [algorithm, complex]
solutions: []
authors: ["broccoliindb"]
weight: 0
disqus_identifier: "46aaf9e9bf5427a325b6c256aa97f04b"
disqus_title: "Complex"
disqus_url: "46aaf9e9bf5427a325b6c256aa97f04b"
---

## 복잡도

탐색을 할 때 따져야하는 부분이 두가지 있다면

|시간복잡도|공간복잡도|
|---|---|
|시간복잡도는 속도의 이슈를 체크|공간복잡도는 메모리의 이슈를 체크|

될수있으면 n^2의 복잡도까지는 가지 않도록 한다.

O(1) < O(logN) < O(N) < O(NlogN) < O(N^2) < O(N^3) < ... < O(2^N) < O(N!)

- 복잡도는 보통 시간 복잡도를 더 많이 떠올린다. 공간복잡도는 하드웨어 성능으로 땜빵할 수 있기 때문이다.
- 일반적으로는 최악의 경우를 연산하지만 알고리즘에 따라서 평균복잡도를 일반적인 복잡도로 여기는 경우도 있다.([Quick Sort](/algorithms/quick-sort))
- 연산의 expression 단위별로 모두 합하면 된다.
