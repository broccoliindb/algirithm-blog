---
title: "Huffman Coding"
displayTitle: ""
date: 2020-11-08T23:31:58+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "무손실 데이터 압축 알고리즘에 하나이다. 문자의 사용빈도에 따른 가변길이의 코드를 문자에 할당한다. 문자에 할당된 가변길이의 코드는 prefix code이다."
tags: [greedy, 탐욕, algorithm, hoffman, 호프만, ASCII]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "c76571f33cdb4a41c6504e75a39888e7"
disqus_title: "Huffman Coding"
disqus_url: "c76571f33cdb4a41c6504e75a39888e7"
---

## Huffman Coding

호프만 코드는 처음 듣는 사람은 한번에 이해하기 어려운 개념이다.
호프만 코드 자체먼저 이해하는 것보다 어떤 개념인지를 먼저 접근해보자.

### 사전지식

호프만 코드를 설명하기 앞서 *ASCII CODE*라는 것을 먼저 설명할 필요가 있다.
왜냐하면 호프만 코드는 ASCII CODE같은 정적인 사이즈의 코드가 아닌 가변적인 사이즈의 코드를 사용하기 때문인데, 깊은 이야기는 꺼내지 않는 게 좋겠다.
여기서 중요한 건 호프만 코드의 대한 *이해*이기 때문에...

#### ASCII CODE

글자정보를 저장하는 코드 방식중 ASCII 가 있는데 이고 `이 코드는 사용자가 사용하는 알파벳의 빈도수를 고려하지 않고 정적인 사이즈(8bit)`를 가지고 있다. 따라서 글자정보를 저장할 때 항상 *정해진 사이즈 만큼 자원*을 소모한다.

`하지만 호프만 코드는 더 자주 사용하는 코드의 대한 빈도를 고려하여 좀더 공간적 자원을 아낄 수 있다`

#### 고정길이코드

컴퓨터와 사람이 서로 소통을 하기 위해 텍스트는 인코딩과 디코딩의 과정이 필요하다. 이 때 *ASCII CODE는 8비트*, *UNICODE는 그 이상의 비트*를 사용해서 인코딩(ex: utf-8)을 한다.

#### Prefix Codes

- `Prefix Codes`는 가변의 길이를 가지고 접두 특징을 가진 코드이다. 이 특징을 가지기 위해서는 한 코드 셋안에서 어떤 코드도 다른 코드의 접두부가 되면 안된다.
- {0, 10, 11}은 이 조건에 *만족*한다. 어떤 코드도 다른 코드의 접두부가 되지 않는다.
- {0,1,10,11}은 이 조건에 *만족하지 않는다.* 왜냐하면 1이 다른 코드들의 접두부가 되기 때문이다.

#### Heap

[Heap](algorithms/heap)은 자료구조 중 하나인데,  Min Heap, Max Heap 방식이 있다. 자세한 내용은 [링크 참조](algorithms/heap)

### Hoffman Code 참고영상

{{< youtube 0kNXhFIEd_w>}}

호프만 코드란?

- 무손실 데이터 압축 알고리즘에 하나이다.
- 문자의 사용빈도에 따른 가변길이의 코드를 문자에 할당한다.
- 문자에 할당된 가변길이의 코드는 prefix code이다.

![hoffman](/algorithms/images/hoffman.png)

### 해결 접근 방법

- 하나의 문자가 노드라 생각하고 Min Heap구조를 만든다.
- 가장 작은 두개의 노드를 선택해서 더 작은것은 왼쪽, 큰쪽은 오른쪽에 두고 두개의 빈도수를 합친 새로운 노드를 선택한 두개의 노드의 루트로 생성해서. Min Heap에 추가한다.
- Min Heap이 하나의 루트 노드를 가지도록 할때까지 위 과정을 반복한다.

![hoffman2](/algorithms/images/hoffman2.png)

#### [참조 : 탐욕알고리즘링크](/algorithms/greedy)