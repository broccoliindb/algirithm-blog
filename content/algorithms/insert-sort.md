---
title: "Insert Sort"
displayTitle: "삽입 정렬"
date: 2020-08-13T14:18:47+09:00
draft: false
isCJKLanguage: true
categories: [sort]
levels: []
description: "앞번째에 있는 수가 더 클경우 큰 수를 하나씩 뒤로 밀면서 작은수를 앞으로 넣는 정렬"
tags: [insert-sort]
solutions:  []
authors: ["broccoliindb"]
weight: 0
disqus_identifier: "913a3ac472802f7af4747f41a3ae885f"
disqus_title: "Insert Sort"
disqus_url: "913a3ac472802f7af4747f41a3ae885f"
---
## 삽입정렬

![insertsort](/algorithms/images/insertsort.png)

복잡도 N^2 으로 좋지 않음. 30건 미만의 작은 건수라면 삽입도 괜춘.

1번째 배열을 정렬 첫 순서로해서 반복한다. 앞번째에 있는 수가 더 클경우 큰 수를 하나씩 뒤로 밀면서 작은수를 앞으로 넣는 정렬

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}
function insertSort(arr) {
  //첫번째 기준
  let i = 1,
    temp,
    j;
  for (i; i < arr.length; i++) {
    temp = arr[i];
    for (j = i - 1; j >= 0 && temp < arr[j]; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = temp;
  }
  console.log(arr);
}
var arr = [5, 6, 1, 3, 4, 2];
insertSort(arr);
{{< /highlight >}}
