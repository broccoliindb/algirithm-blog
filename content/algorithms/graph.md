---
title: "Graph"
displayTitle: "그래프"
date: 2020-08-13T16:28:44+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "그래프는 노드들간에 연결점이 그려진 군집체이다. 이때 이 연결점은 방향도 있을수있고 중첩도 있을수있다."
tags: [algorithm, graph, bfs, dfs]
solutions: []
authors: ["broccoliindb"]
weight: 0
disqus_identifier: "dbf4eb5b96807cb9886b99c5055a91e7"
disqus_title: "Graph"
disqus_url: "dbf4eb5b96807cb9886b99c5055a91e7"
---
## 그래프

그래프는 노드들간에 연결점이 그려진 군집체이다. 이때 이 연결점은 방향도 있을수있고 중첩도 있을수있다.

노드를 다른말로 버텍스(vertex)라고도 한다.
연결점을 간선 혹은 엣지(edge)라고도 한다.
여러개의 간선이 있을경우 차수(degree)로 표현한다.

그래프를 구현할 때 인접행렬, 인접리스트 두가지 형태로 구현할 수 있다.

### **문제 접근방법**

특정 좌표가 주어지고 그 좌표의 연결점들이 주어진다고 했을때
좌표는 `노드`이고 연결점들은 `edgeList`이다.

> 이때 이 edgeList를 가지고 **인접행렬** 또는 **인접리스트**를 만들어서 그래프를 탐색해야한다.

#### 인접행렬

인접행렬을 만들때는 각 노드들이 어떤 노드에 연결되어있는지에 따라 2차행렬을 만든다. 이방식은 **인접여부를 판단할때는 빠르나 공간적인 낭비가 있을 수 있다. 그리고 모든인접공간을 탐색해야한다.**

- 장점 : 인접여부만을 판단할때 빠름 : O(1)
  ```
  a[i][j] === 1 ? Y : N
  ```
- 단점 : 모든 노드(버텍스)를 스퀘어로 다 탐색해야하기 때문에 탐색시 느림 : O(N^2)

#### 인접리스트

인접리스트는 총 수가 최대 **O(N)** 개가 생성된다. 따라서 **탐색시간이 좀 더 효율적이지만 인접판단이 탐색방법에 따라 오래걸리수 있다.**

- 장점 : 탐색 노드 수가 많아도 O(N)의 시간이 소요됨.
- 단점: 인접여부를 판단할 때 느림 : O(E)
  - 엣지(아크)의 수의 따라 시간이 걸림

### 인접리스트의 탐색방법

#### 참고영상

{{< youtube _hxFgg7TLZQ>}}

### BFS, Breadth First Search

BFS는 시작점의 `가장 가까운 노드부터` 모두 방문한 다음에 `가장 깊은 쪽`에 있는 노드를 가장 마지막에 접근하는 방식이다. 같은 depth의 있는 노드들의 방문순서는 정해진것이 없으므로 탐색순서는 어떻게 방문했냐에 따라 달라질 수 있다.

이때 방문한다는 것은 어떤 공간에 담는다 했을 때 시작점에 가까운 노드들이 먼저 방문되고 나가는 방식이므로 **_queue_** 에 담는다고 보면 된다. 이 **_queue_** 에 담기는 순간에는 이미 방문한 노드이므로 방문했다는 **_flag_** 를 줘야한다.

{{< highlight "js" "linenos=table,linenostart=1" >}}
let queue = []
let result = []
let visited = new Map()
//start는 시작점
queue.push(start)
visited.set(start, true)
while(queue.length) {
  const first = queue[0]
  result.push(queue.shift())
  //adjacencyList는 인접리스트로 이미 만들었다 가정함
  if (adjacencyList.get(first) && adjacencyList.get(first).length) {
    adjacencyList.get(first).forEach(node => {
      if (!visited.get(node)) {
        queue.push(node)
        visited.set(node, true)
      }
    })
  }
}
console.log(result.join(' '))
{{< /highlight >}}

#### DFS, Depth First Search

DFS는 방문했던 `노드의 가장 깊은 쪽까지 모두 방문한 다음에` `시작점 이웃노드부터 다시 그 노드의 가장 깊은곳까지 방문`하는 순서로 탐색하는 방법이다.
마찬가지로 같은 depth의 노드들의 방문순서는 정해진것이 없으므로 탐색순서는 어떻게 방문했냐에 따라 달라질 수 있다.

이때 방문한다는 것은 어떤 공간에 담는다 했을 때 시작점에 가까운 노드들이 먼저 방문되지만 깊은위치의 노드들이 먼저 나가는 방식이므로 **_stack_** 에 담는다고 보면 된다. 이 **_stack_** 에 담기는 순간에는 이미 방문한 노드이므로 방문했다는 **_flag_** 를 줘야한다.

{{< highlight "js" "linenos=table,linenostart=1" >}}
let stack = []
let result = []
let visited = new Map()
const dfs = (start) => {
  while(stack.length) {
    const last = stack[stack.length - 1]
    //adjacencyList는 인접리스트로 이미 만들었다 가정함
    result.push(stack.pop())
    if (adjacencyList.get(last) && adjacencyList.get(last).length) {
      for (const node of adjacencyList.get(last)) {
        if (!visited.get(node)) {
          stack.push(node)
          visited.set(node, true)
          // dfs는 재귀를 사용하는것이 간편함
          dfs(node)
        }
      }
    }
  }
}
//start는 시작점
stack.push(start)
visited.set(start, true)
dfs(start)
console.log(result.join(' '))

{{< /highlight >}}

### Graph 구현

#### Graph 탐색 참고문제

[🔗 백준 1260 DFS와 BFS](/problems/graph/)

#### Graph js 구현

- 그래프는 `노드`를 추가하거나 삭제할수있다.
- 그래프는 `간선`을 추가하거나 삭제할수있다.

{{< highlight "js" "linenos=table,linenostart=1" >}}
class Graph {
  constructor() {
    this.nodeList = new Map()
    this.adjacencyList = new Map()
  }

  static addNode (graph, nodeToAdd) {
    if (!graph.nodeList.has(nodeToAdd)) {
      graph.nodeList.set(nodeToAdd, false)
    }
  }

  static deleteNode (graph, nodeToDelete) {
    if (graph.nodeList.has(nodeToDelete)) {
      // 인접리스트에 없는 항목이어야 삭제할 수 있다.
      if (graph.adjacencyList.get(nodeToDelete) && graph.adjacencyList.get(nodeToDelete).length) return
      graph.nodeList.delete(nodeToDelete)
    }
  }

  // 간선의 중첩은 다루지 않음 만약 다룬다면 오브젝트로 다뤄야함.
  static addEdge (graph, preNode, postNode, isOneWay) {
    if (!graph.adjacencyList.has(preNode)) {
      graph.adjacencyList.set(preNode, [postNode])
    } else {
      graph.adjacencyList.get(preNode).push(postNode)
    }
    if (!isOneWay) {
      if (!graph.adjacencyList.has(postNode)) {
        graph.adjacencyList.set(postNode, [preNode])
      } else {
        graph.adjacencyList.get(postNode).push(preNode)
      }
    }
  }

  static deleteEdge (graph, preNode, postNode, isOneWay) {
    if (!graph.adjacencyList.has(preNode) || !graph.adjacencyList.has(postNode)) return
    const index = graph.adjacencyList.get(preNode).indexOf(postNode)
    graph.adjacencyList.get(preNode).splice(index, 1)
    if (!isOneWay) {
      const index = graph.adjacencyList.get(postNode).indexOf(postNode)
      graph.adjacencyList.get(postNode).splice(index, 1)
    }
  }
}

const g = new Graph()
Graph.addNode(g, 'X')
Graph.addNode(g, 'G')
Graph.addNode(g, 'H')
Graph.addNode(g, 'P')
Graph.addNode(g, 'E')
Graph.addNode(g, 'M')
Graph.addNode(g, 'Y')
Graph.addNode(g, 'J')
Graph.addEdge(g, 'A', 'X')
Graph.addEdge(g, 'X', 'G')
Graph.addEdge(g, 'X', 'H')
Graph.addEdge(g, 'G', 'H')
Graph.addEdge(g, 'G', 'P')
Graph.addEdge(g, 'H', 'E')
Graph.addEdge(g, 'H', 'P')
Graph.addEdge(g, 'E', 'M')
Graph.addEdge(g, 'E', 'Y')
Graph.addEdge(g, 'Y', 'M')
Graph.addEdge(g, 'M', 'J')
console.log(g.nodeList)
console.log(g.adjacencyList)
{{< /highlight >}}
