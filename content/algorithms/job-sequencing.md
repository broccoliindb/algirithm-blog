---
title: "Job Sequencing"
displayTitle: "Job Sequencing"
date: 2020-11-08T23:32:20+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "Job의 대한 정보가 N개로 주어지고 어떤 Job에 최소로 주어진 기한은 한개 unit의 time이다. 한번에 한개의 Job을 수행할 때 이윤을 최대로 뽑아먹는 방법"
tags: [greedy, 탐욕, algorithm]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "0b1fe1cf64ea7b76d7a8f5d257ffe782"
disqus_title: "Job Sequencing"
disqus_url: "0b1fe1cf64ea7b76d7a8f5d257ffe782"
---

## Job Sequencing Problem

### 참고영상

{{< youtube R6Skj4bT1HE>}}

모든 Job은 기한이 이고 기한 전에 마무리하는것이 이윤을 남기는 방법이다.

Job의 대한 정보가 N개로 주어지고 어떤 Job을 수행할때 소모되는 시간은 한개 unit의 time이다. 한번에 한개의 Job을 수행할 때 이윤을 최대로 뽑아먹는 방법

### 해결 접근 방법

![job-sequencing](/algorithms/images/job-sequencing.png)

- 이윤에 따라 job을 sorting한다.
- 첫번째 job을 시작으로 결과값 배열에 넣는다.
- 현재 job이 deadline을 맞추면서도 결과값배열에 넣을 수 있다면 넣고 아니면 무시해라.

#### [참조 : 탐욕알고리즘링크](/algorithms/greedy)