---
title: "Greedy"
displayTitle: "욕심쟁이(Greedy) Algorithm"
date: 2020-11-08T23:15:06+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "매 선택의 순간마다 논리적으로 최선의 선택을 만드는것이 전체적으로 최선의 결과이길 바라는 알고리즘."
tags: [greedy, 탐욕, algorithm]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "59782b037a8fbc571fa483f18ecbe895"
disqus_title: "Greedy"
disqus_url: "59782b037a8fbc571fa483f18ecbe895"
---
## Greedy Algorithm

### 참고영상

{{< youtube HzeK7g8cD0Y>}}

### 영상요약 정리

an algorithmic paradigm that follows the problem solving approach of making th locally optimal choice at each stage with the hope of finding a global optimum.

매 선택의 순간마다 논리적으로 최선의 선택을 만드는것이 전체적으로 최선의 결과이길 바라는 알고리즘

- 장점: 구조가 간단, 구현이 용이
- 단점: `전체적으로 봤을때 최선의 결과가 아닌경우가 많음`

![greedy-algorithm-ex](/algorithms/images/greedy.png)

**`그리디 알고리즘으로는 원하는 답을 얻지 못 할 수 있음.`**

### 그리디 알고리즘 적용

전체적인 최적은 각 작은 단위별 최적에 의해 도달하게 되는 로직일 경우

- [Activity Selection Problem](/algorithms/activity-selection-problem)
- [Huffman Coding](/algorithms/huffman-coding)
- [Job Sequencing Problem](/algorithms/job-sequencing)
- [Fractional Knapsack Problem](/algorithms/fractional-knapsack)
- [Prim's Minimum Spanning Tree](/algorithms/prims-minimum-spanning)