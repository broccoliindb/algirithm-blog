---
title: "Bubble Sort"
displayTitle: "버블 정렬"
date: 2020-08-13T14:17:50+09:00
isCJKLanguage: true
draft: false
categories: [sort]
levels: []
description: "버블정렬은 인접한 두 수를 비교해서 작은수가 뒤에있으면 서로 swap 해준다."
tags: [bubble-sort]
solutions:  []
authors: ["broccoliindb"]
weight: 0
disqus_identifier: "d1cfb891b6843f783b895b50d7afdfd0"
disqus_title: "Bubble Sort"
disqus_url: "d1cfb891b6843f783b895b50d7afdfd0"
---
## 버블정렬  

![bubblesort](/algorithms/images/bubblesort.png)

N^2 중에서도 성능안좋음.

버블정렬은 인접한 두 수를 비교해서 작은수가 뒤에있으면 서로 swap 해준다.

`정렬모양이 버블`이라서 **버블정렬**임.

- 첫번째 for의 반복 횟수는 arr.length - 1 ( 첫번째 돌때 i는 n-1까지만 가면 됨. 마지막아이템은 전에 것과 비교되므로.)
- 두번째 for의 반복 횟수는 arr.length - 1 - i (매 횟수마다 맨 마지막 아이템은 이미 모든 놈들과 비교한 놈이므로 n - 1 - i가 됨.) 

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}
function bubbleSort(arr) {
  let i = 0,
    temp;
  for (i; i < arr.length - 1; i++) {
    for (let j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        temp = arr[j + 1];
        arr[j + 1] = arr[j];
        arr[j] = temp;
      }
    }
  }
  return arr;
}
var arr = [5, 4, 2, 1, 3];
bubbleSort(arr);
{{< /highlight >}}