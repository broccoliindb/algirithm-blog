---
title: "Activity Selection Problem"
displayTitle: "Activity Selection Problem"
date: 2020-11-08T23:31:44+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "N개의 활동이 주어지고 그 활동의 시작과 끝시간이 주어질때 한사람이 수행할 수 있는 가장 많은 활동의 수를 구해라."
tags: [greedy, 탐욕, algorithm, 활동선택문제, "activity selection problem"]
solutions: []
authors: [broccoliindb]
weight: 0
disqus_identifier: "b6465cf4af90130a92ab095cbb539273"
disqus_title: "Activity Selection Problem"
disqus_url: "b6465cf4af90130a92ab095cbb539273"
---

## Activity Selection Problem

### 참고영상

{{< youtube poWB2UCuozA>}}

N개의 활동이 주어지고 그 활동의 시작과 끝시간이 주어질때 한사람이 수행할 수 있는 가장 많은 활동의 수를 구해라.

### 해결 접근 방법

- activity의 종료시간에 따른 sorting
- 첫번째 activity선택하고 sorting 순서대로 활동 진행하되
  - `이번 활동의 시작시간이 이전활동의 종료시간보다 크거나 같다면 진행한다.`

#### [참조 : 탐욕알고리즘링크](/algorithms/greedy)