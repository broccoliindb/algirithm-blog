---
title: "Two Pointer"
displayTitle: "투 포인터"
date: 2020-09-29T11:16:58+09:00
isCJKLanguage: true
draft: false
categories: [algorithm]
levels: []
description: "하나의 배열형태로 이루어진 데이터를 탐색할 때 한번만 루프를 돌면서 원하는 결과를 얻을때 사용함"
tags: ["two pointer"]
solutions: []
authors: []
weight: 0
disqus_identifier: "00293bf5a1dcfedf411f86026f8f4129"
disqus_title: "Two Pointer"
disqus_url: "00293bf5a1dcfedf411f86026f8f4129"
---

## 투 포인터

Two Pointer Technique 참고영상

{{< youtube 2wVjt3yhGwg >}}

ℹ️ 영상 요약

- 하나의 배열형태로 이루어진 데이터를 탐색할 때 한번만 루프를 돌면서 원하는 결과를 얻을때 사용하는 기술이다.  
- 따라서 시작복잡도는 O(n)되고 혹은 데이터의 소팅이 필요시에는 O(NLogN)이 될 수도 있다.
- 활용방법은 start와 end가 모두 0부터 시작하는 `Equi-directional`과, start, end가 각각 0, N-1에서 시작하는 `Opposite directional`이 있다.
  - Equi-directional: start는 slow-runner, end는 fast-runner이며 start <= end, end < n 일때 까지 반복된다.
  - Opposite-directional: 양 끝에서 서로를 만나거나 특정 조건이 만족되었을 때 끝난다.

### Pseudo Code 

ℹ️ Opposite-directional

보통 소팅이 필요한 경우에 사용하면 될거라고 생각함.
```js
시작점 start : 0
마지막 end: arr.length - 1
조건변수 : sum
조건만족변수 : M
결과 count: 0
while(start < end) {
  sum = arr[start] + arr[end]
  if ( sum === M ) {
    count++
    end--
  } else if ( sum > M) {
    end--
  } else {
    start++
  }
}
return count
```

ℹ️ Equi-directional

소팅이 필요없는 부분합과 같은 과정에서 필요하다고 생각함.

```js
시작점 start : 0
마지막 end: 0
조건변수 sum : arr[end]
조건만족변수 : M
결과 count: 0
while(start <= end && end < arr.length) {
  if ( sum === M ) {
    count++
    end++
    sum = sum + arr[end] - arr[start]
    start++
  } else if ( sum > M) {
    sum -= arr[start]
    start++
  } else {
    end++
    sum += arr[end]
  }
}
return count
```