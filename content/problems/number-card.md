---
title: "Number Card"
displayTitle: "숫자 카드"
date: 2020-10-07T04:50:33+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "숫자 카드를 상근이가 가지고 있는지 아닌지를 구하는 프로그램을 작성하시오."
tags: [binary-search]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "6742196279fcfadb49012fd57216225a"
disqus_title: "Number Card"
disqus_url: "6742196279fcfadb49012fd57216225a"
---

## 숫자 카드

{{< problem >}}
문제
{{< /problem >}}

숫자 카드는 정수 하나가 적혀져 있는 카드이다. 상근이는 숫자 카드 N개를 가지고 있다. 정수 M개가 주어졌을 때, 이 수가 적혀있는 숫자 카드를 상근이가 가지고 있는지 아닌지를 구하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}
첫째 줄에 상근이가 가지고 있는 숫자 카드의 개수 N(1 ≤ N ≤ 500,000)이 주어진다. 둘째 줄에는 숫자 카드에 적혀있는 정수가 주어진다. 숫자 카드에 적혀있는 수는 -10,000,000보다 크거나 같고, 10,000,000보다 작거나 같다. 두 숫자 카드에 같은 수가 적혀있는 경우는 없다.

셋째 줄에는 M(1 ≤ M ≤ 500,000)이 주어진다. 넷째 줄에는 상근이가 가지고 있는 숫자 카드인지 아닌지를 구해야 할 M개의 정수가 주어지며, 이 수는 공백으로 구분되어져 있다. 이 수도 -10,000,000보다 크거나 같고, 10,000,000보다 작거나 같다

```
5
6 3 2 10 -10
8
10 9 -5 2 3 4 5 -10
```


{{< output >}}
출력
{{< /output >}}
첫째 줄에 입력으로 주어진 M개의 수에 대해서, 각 수가 적힌 숫자 카드를 상근이가 가지고 있으면 1을, 아니면 0을 공백으로 구분해 출력한다.

```
1 0 0 1 1 0 0 1
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const input = require('fs').readFileSync('/dev/stdin')
const data = input.toString().trim().split(/\n+/).map(i => i.toString().trim().split(/\s+/).map(n => +n))
const N = data[0]
const A = data[1].sort((a, b) => a - b)
const M = data[2]
const targetArr = data[3]
const results = []
const getResult = (arr, s, e, target) => {
  let mid = Math.floor((s + e) / 2)
  if (s <= e) {
    if (target === arr[mid]) {
      return 1
    } else if (target > arr[mid]) {
      return getResult(arr, ++mid, e, target)
    } else {
      return getResult(arr, s, --mid, target)
    }
  }
  return 0
}

for (let i = 0; i < M; i++) {
  const result = getResult(A, 0, N - 1, targetArr[i])
  results.push(result)
}
console.log(results.join(' '))
{{< /highlight >}}