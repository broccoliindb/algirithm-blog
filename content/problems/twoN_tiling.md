---
title: "TwoN_tiling"
displayTitle: "2 X N 타일링"
date: 2020-11-04T14:12:06+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "2×n 크기의 직사각형을 1×2, 2×1 타일로 채우는 방법의 수를 구하는 프로그램을 작성하시오."
tags: [memoization, dynamic]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "121c1a7b03157106b7199a658be2ea9f"
disqus_title: "TwoN_tiling"
disqus_url: "121c1a7b03157106b7199a658be2ea9f"
---

## 2 X N 타일링

{{< problem >}}
문제
{{< /problem >}}

2×n 크기의 직사각형을 1×2, 2×1 타일로 채우는 방법의 수를 구하는 프로그램을 작성하시오.

아래 그림은 2×5 크기의 직사각형을 채운 한 가지 방법의 예이다.

![twoN_tiling](/problems/images/2N_tiling.png)

{{< input >}}
입력
{{< /input >}}

첫째 줄에 n이 주어진다. (1 ≤ n ≤ 1,000)

{{< output >}}
출력
{{< /output >}}

첫째 줄에 2×n 크기의 직사각형을 채우는 방법의 수를 10,007로 나눈 나머지를 출력한다.

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const T = (N, save) => {
  if (N === 1) {
    save[N] = 1;
    return save[N];
  }
  if (N === 2) {
    save[N] = 2;
    return save[N];
  }
  if (N === 3) {
    save[N] = 3;
    return save[N];
  }
  if (N >= 4) {
    const tempN_1 = save[N - 1] || T(N - 1, save);
    const tempN_2 = save[N - 2] || T(N - 2, save);
    save[N] = (tempN_1 + tempN_2) % 10007;
    return save[N];
  }
};

const getResult = (N) => {
  const save = {};
  return T(N, save);
};
console.log(getResult(N));

{{< /highlight >}}

위에 그림이 흐릿하지만...저게 원본이미지이다. ㅠ 자세히 보아야 보인다.
`메모이제이션 문제는 문제의 규칙을 찾는게 곧 답`인문제인데, 규칙을 찾는게 항상 나는 쉽지는 않다. 하지만 천천히 인내심을 가지고 보면 보인다!!!
