---
title: "Tree"
displayTitle: "트리"
date: 2020-08-09T18:25:13+09:00
draft: false
isCJKLanguage: true
category: [basic]
levels: [level1]
description: "프로그램으로 트리모양 출력하기"
tags: []
solutions: ["completed"]
authors: ["broccoliindb"]
weight: 0
disqus_identifier: "981c120e0dc5e413c8b4971c3fba8ce1"
disqus_title: "Tree"
disqus_url: "981c120e0dc5e413c8b4971c3fba8ce1"
---

## 프로그래밍으로 트리 만들기

{{< problem >}}
문제
{{< /problem >}}

프로그래밍으로 트리를 만들기

{{< input >}}
입력
{{< /input >}}

양의정수

{{< output >}}
출력
{{< /output >}}

```js
    *
   ***
  *****
 *******
*********
```

{{< solution >}}
#1. 풀이확인 js
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

function drawStar(input) {
    const length = getLength(input)
    for (let i = 0; i < input; i++) {
        const result = setStar(length, i)
        console.log(result)
    }
}

function getLength(input) {
    let length = 1
    for (let i = 1; i < input; i++) {
    length = length + 2
    }
    return length
}

function setStar(length, index) {
    let midArr = []
    const mid = parseInt(length / 2)
    let count = 2 * index + 1
    let previousArr = Array(mid).fill(' ')
    let postArr = Array(mid).fill(' ')
    while (count--) {
    midArr.push('*')
    }
    previousArr.splice(0, parseInt(midArr.length / 2))
    postArr.splice(0, parseInt(midArr.length / 2))

    let result = previousArr.concat(midArr).concat(postArr)
    return result.join('')
}
drawStar(10)

{{< /highlight >}}
