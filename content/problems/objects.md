---
title: "Objects"
displayTitle: "객체만들기"
date: 2020-08-13T05:11:27+09:00
draft: false
isCJKLanguage: true
categories: [basic]
levels: [level1]
description: "입력 두개를 받아 출력하는 객체 만들기"
tags: []
solutions: ["completed"]
authors: ["broccoliindb"]
weight: 0
disqus_identifier: "f7616b1c08e6c8b565cad18bf0cada33"
disqus_title: "Objects"
disqus_url: "f7616b1c08e6c8b565cad18bf0cada33"
---

## 입력 두개를 받아 출력하는 객체 만들기

{{< problem >}}
문제
{{< /problem >}}

이름과 점수를 각각 따로 입력받아서 이름과 점수를 매핑하는 object를 출력하라.

{{< input >}}
입력
{{< /input >}}

```js
입력 1: mike jonathan
입력 2: 39 54
```

{{< output >}}
출력
{{< /output >}}

```js
{
  mike: 39,
  jonathan: 54
}
```

{{< solution >}}
#1. 풀이확인
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}
const nameArr = []
const students =  {}
const setNames = (...names) => {
  names.forEach(name => {
    nameArr.push(name)
  })
}
const setMathScores = (...mathScores) => {
  mathScores.forEach((math, index) => {
    students[nameArr[index]] = math
  })
}

const names = ["mike", "jonathan"]
const scores = ["39","54"]
setNames(...names)
setMathScores(...scores)
console.log(students)
{{< /highlight >}}