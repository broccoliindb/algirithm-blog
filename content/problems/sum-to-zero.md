---
title: "Sum to Zero"
displayTitle: "합이 0인 네 정수"
date: 2020-10-03T23:37:30+09:00
isCJKLanguage: true
draft: false
categories: [2차배열]
levels: []
description: "합이 0인 네 정수의 경우의 수 구하기"
tags: []
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "43ede9d94ff6a42694ae2797130d458b"
disqus_title: "Sum to Zero"
disqus_url: "43ede9d94ff6a42694ae2797130d458b"
---

## 부분합

{{< problem >}}
문제
{{< /problem >}}

정수로 이루어진 크기가 같은 배열 A, B, C, D가 있다.

A[a], B[b], C[c], D[d]의 합이 0인 (a, b, c, d) 쌍의 개수를 구하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 배열의 크기 n (1 ≤ n ≤ 4000)이 주어진다. 다음 n개 줄에는 A, B, C, D에 포함되는 정수가 공백으로 구분되어져서 주어진다. 배열에 들어있는 정수의 절댓값은 최대 228이다.

```js
6
-45 22 42 -16
-41 -27 56 30
-36 53 -37 77
-36 30 -75 -46
26 -38 -10 62
-32 -54 -6 45
```

{{< output >}}
출력
{{< /output >}}

합이 0이 되는 쌍의 개수를 출력한다.

```js
5
```

{{< solution >}}
#1. 풀이확인 js
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

function getResult(subSum) {
  let rst = {}
  let N = +subSum[0]
  i = N
  while (i >= 1) {
    let j = N
    while (j >= 1) {
      const sum = subSum[i][2] + subSum[j][3]
      rst[sum] ? ++rst[sum] : rst[sum] = 1
      j--
    }
    i--
  }

  i = N
  let count = 0
  while (i >= 1) {
    let j = N
    while (j >= 1) {
      const sum = - (subSum[i][0] + subSum[j][1])
      if (rst[sum]) count += rst[sum]
      j--
    }
    i--
  }

  console.log(count)
}

let input = require("fs").readFileSync('/dev/stdin');

const s = new Date()
getResult(input.toString().trim().split(/\n+/).map(a => a.trim().split(/\s+/).map(n => +n)))
const e = new Date()
console.log(e - s)

{{< /highlight >}}

백준에서 통과는 하지 못했다. 시간초과로 통과하지 못했는데, 이유는 잘 모르겠다.(4000라인까지 해봤을 때 내 터미널에선 2초가 걸리진 않았다.)

이 문제를 해결하기 위해서 이분탐색을 처음에는 활용했었는데, 해당 방법보다는
오브젝트의 키값을 활용한 방식이 훨씬 효율적이라 관련 로직은 제거함.

 > 시간초과문제로 readline에서 readFileSync방식으로 변경

 > for문을 while로 대체해봤는데, 사실 별차이 없었음.
 
 > ++ 보다 --가 빠르다고해서 해봤는데 사실 별차이 없었음.