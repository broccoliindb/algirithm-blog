---
title: "Rest Money"
displayTitle: "거스름돈"
date: 2020-11-11T18:53:43+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "물건을 사고 카운터에서 1000엔 지폐를 한장 냈을 때, 받을 잔돈에 포함된 잔돈의 개수를 구하는 프로그램을 작성하시오."
tags: [greedy]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "7073a1fc872f1e4d6dd5707d3e21dd91"
disqus_title: "Rest Money"
disqus_url: "7073a1fc872f1e4d6dd5707d3e21dd91"
---

## 거스름돈

{{< problem >}}
문제
{{< /problem >}}

타로는 자주 JOI잡화점에서 물건을 산다. JOI잡화점에는 잔돈으로 500엔, 100엔, 50엔, 10엔, 5엔, 1엔이 충분히 있고, 언제나 거스름돈 개수가 가장 적게 잔돈을 준다. 타로가 JOI잡화점에서 물건을 사고 카운터에서 1000엔 지폐를 한장 냈을 때, 받을 잔돈에 포함된 잔돈의 개수를 구하는 프로그램을 작성하시오.

예를 들어 입력된 예1의 경우에는 아래 그림에서 처럼 4개를 출력해야 한다.

{{< input >}}
입력
{{< /input >}}

입력은 한줄로 이루어져있고, 타로가 지불할 돈(1 이상 1000미만의 정수) 1개가 쓰여져있다.

{{< output >}}
출력
{{< /output >}}

제출할 출력 파일은 1행으로만 되어 있다. 잔돈에 포함된 매수를 출력하시오.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
380
//출력1
4
```

{{< solution >}}
#1. 풀이확인 js 
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

const N = parseInt(
  require('fs').readFileSync('/dev/stdin').toString().trim(),
  10
)
const data = [500, 100, 50, 10, 5, 1]

const main = () => {
  let rest = 1000 - N
  let count = 0
  for (let i = 0; i < data.length; i++) {
    if (rest === 0) {
      break
    }
    const share = Math.floor(rest / data[i])
    if (share !== 0) {
      count += share
      rest = rest % data[i]
    }
  }
  console.log(count)
}
main()
{{< /highlight >}}
