---
title: "Sum Prime"
displayTitle: "소수의 연속합"
date: 2020-09-30T05:15:44+09:00
isCJKLanguage: true
draft: false
categories: [algorithm]
levels: []
description: "특정 자연수가 소수의 연속합인 경우구하기"
tags: ["two pointer"]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "903db3f86d78ccf6ad49fb0d82caf26c"
disqus_title: "Sum Prime"
disqus_url: "903db3f86d78ccf6ad49fb0d82caf26c"
---

## 소수의 연속합

{{< problem >}}
문제
{{< /problem >}}

하나 이상의 연속된 소수의 합으로 나타낼 수 있는 자연수들이 있다. 몇 가지 자연수의 예를 들어 보면 다음과 같다.

- 3 : 3 (한 가지)
- 41 : 2+3+5+7+11+13 = 11+13+17 = 41 (세 가지)
- 53 : 5+7+11+13+17 = 53 (두 가지)

하지만 연속된 소수의 합으로 나타낼 수 없는 자연수들도 있는데, 20이 그 예이다. 7+13을 계산하면 20이 되기는 하나 7과 13이 연속이 아니기에 적합한 표현이 아니다. 또한 한 소수는 반드시 한 번만 덧셈에 사용될 수 있기 때문에, 3+5+5+7과 같은 표현도 적합하지 않다.

자연수가 주어졌을 때, 이 자연수를 연속된 소수의 합으로 나타낼 수 있는 경우의 수를 구하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 자연수 N이 주어진다. (1 ≤ N ≤ 4,000,000)

{{< output >}}
출력
{{< /output >}}

첫째 줄에 자연수 N을 연속된 소수의 합으로 나타낼 수 있는 경우의 수를 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
20
//출력1
0
//입력2
3
//출력2
1
//입력3
41
//출력3
3
//입력4
53
//출력4
2
```

{{< solution >}}
#1. 풀이확인 js
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

const readline = require('readline')
const r = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const isPrime = (N) => {
  for (let i = 3; i <= Math.floor(Math.sqrt(N)); i += 2) {
    if (N % i === 0) {
      return false
    }
  }
  return true
}

const getArr = (N) => {
  const arr = []
  if (N >= 2) {
    arr.push(2)
    if (N >= 3) {
      for( let i = 3; i <= N; i +=2 ) {
        if (isPrime(i)) {
          arr.push(i)
        }
      }
    }
  }
  // console.log(arr)
  return arr
}


r.on('line', line => {
  const N = parseInt(line.toString().trim().split(' ')[0], 10)
  const arr = getArr(N)
  let start = 0
  let end = 0
  let count = 0
  let sum = arr[end]

  while( start <= end && end < arr.length) {
    // console.log(start, end, sum, N)
    if ( sum === N) {
      count++
      end++
      sum = sum + arr[end] - arr[start]
      start++ 
    } else if ( sum < N) {
      end++
      sum = sum + arr[end]
    } else {
      sum = sum - arr[start]
      start++
    }
  }
  console.log(count)
}).on('close', ()=>{
  process.exit()
})


{{< /highlight >}}
