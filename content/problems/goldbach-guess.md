---
title: "Goldbach Guess"
displayTitle: "골드바흐의 추측"
date: 2020-10-11T23:16:23+09:00
isCJKLanguage: true
draft: false
categories: [math]
levels: []
description: "4보다 큰 모든 짝수는 두 홀수 소수의 합으로 나타내는 골드바흐의 추측을 검증하는 프로그램만들기"
tags: []
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "19f2c1fd63ed3ccabefadf6af31a18b3"
disqus_title: "Goldbach Guess"
disqus_url: "19f2c1fd63ed3ccabefadf6af31a18b3"
---

## 골드바흐의 추측

{{< problem >}}
문제
{{< /problem >}}


1742년, 독일의 아마추어 수학가 크리스티안 골드바흐는 레온하르트 오일러에게 다음과 같은 추측을 제안하는 편지를 보냈다.

4보다 큰 모든 짝수는 두 홀수 소수의 합으로 나타낼 수 있다.
예를 들어 8은 3 + 5로 나타낼 수 있고, 3과 5는 모두 홀수인 소수이다. 또, 20 = 3 + 17 = 7 + 13, 42 = 5 + 37 = 11 + 31 = 13 + 29 = 19 + 23 이다.

이 추측은 아직도 해결되지 않은 문제이다.

백만 이하의 모든 짝수에 대해서, 이 추측을 검증하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

입력은 하나 또는 그 이상의 테스트 케이스로 이루어져 있다. 테스트 케이스의 개수는 100,000개를 넘지 않는다.

각 테스트 케이스는 짝수 정수 n 하나로 이루어져 있다. (6 ≤ n ≤ 1000000)

입력의 마지막 줄에는 0이 하나 주어진다.

```
8
20
42
0
```

{{< output >}}
출력
{{< /output >}}

각 테스트 케이스에 대해서, n = a + b 형태로 출력한다. 이때, a와 b는 홀수 소수이다. 숫자와 연산자는 공백 하나로 구분되어져 있다. 만약, n을 만들 수 있는 방법이 여러 가지라면, b-a가 가장 큰 것을 출력한다. 또, 두 홀수 소수의 합으로 n을 나타낼 수 없는 경우에는 "Goldbach's conjecture is wrong."을 출력한다.

```
8 = 3 + 5
20 = 3 + 17
42 = 5 + 37
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
let input = require('fs').readFileSync('/dev/stdin')
let data = input.toString().trim().split(/n+/).map(i => i.toString().trim().split(/\s+/).map(n => +n))[0]

const getHalfPrimeList = () => {
  const halfPrimeList = []
  for (let i = 3; i <= Math.floor(1000000 / 2); i = i + 2) {
    let isPrime = true
    for (let j = 3; j <= Math.floor(Math.sqrt(i)); j = j + 2) {
      if (i % j === 0) {
        isPrime = false
      }
    }
    if (isPrime) {
      halfPrimeList.push(i)
    }
  }
  return halfPrimeList
}

const isPrime = (n) => {
  let isPrime = true
  for (let x = 3; x <= Math.floor(Math.sqrt(n)); x = x + 2) {
    if (n % x === 0) {
      isPrime = false
    }
  }
  return isPrime
}

const getResult = (n, primeList) => {
  if (primeList.length) {
    for (let i = 0; i < primeList.length; i++) {
      const x = n - primeList[i]
      if (isPrime(x)) {
        console.log(`${n} = ${primeList[i]} + ${x}`)
        break
      }
      if (i === primeList.length - 1) {
        console.log(`Goldbach's conjecture is wrong.`)
      }
    }
  }
}

const main = () => {
  const halfPrimeList = getHalfPrimeList()//
  for (let i = 0; i < data.length; i++) {
    if (data[i] === 0) return
    getResult(data[i], halfPrimeList)
  }
}

main()
{{< /highlight >}}

처음 몇번 시도할 때 메모리 초과가 떠서 어떻게 하면 메모리를 줄일까 고민하면서 풀었다.

결과적으로는 맥스값의 홀수값들 중 절반까지만 소수판별을 해서 리스트에 넣고 그 절반값을가지고 나온 다른 값이 소수인지 판별해서 로그를 출력하도록 했다.

결국 해결은 했는데, 에라토스의 체를 미리 알고 그것을 썼다면 메모리는 이것보다 훨씬 더 나왔겠지만 소스상으로는 더 간략하게 나올수있었을텐데하고 생각한다.
