---
title: "Arrange Meeting Room"
displayTitle: "회의실배정"
date: 2020-11-09T15:32:36+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "사용할 수 있는 회의의 최대 개수를 출력한다."
tags: [greed, 탐욕]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "09c1cf46077fd1d8e3b22aa965505677"
disqus_title: "Arrange Meeting Room"
disqus_url: "09c1cf46077fd1d8e3b22aa965505677"
---

## Greedy

{{< problem >}}
문제
{{< /problem >}}

한 개의 회의실이 있는데 이를 사용하고자 하는 N개의 회의에 대하여 회의실 사용표를 만들려고 한다. 각 회의 I에 대해 시작시간과 끝나는 시간이 주어져 있고, 각 회의가 겹치지 않게 하면서 회의실을 사용할 수 있는 회의의 최대 개수를 찾아보자. 단, 회의는 한번 시작하면 중간에 중단될 수 없으며 한 회의가 끝나는 것과 동시에 다음 회의가 시작될 수 있다. 회의의 시작시간과 끝나는 시간이 같을 수도 있다. 이 경우에는 시작하자마자 끝나는 것으로 생각하면 된다.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 회의의 수 N(1 ≤ N ≤ 100,000)이 주어진다. 둘째 줄부터 N+1 줄까지 각 회의의 정보가 주어지는데 이것은 공백을 사이에 두고 회의의 시작시간과 끝나는 시간이 주어진다. 시작 시간과 끝나는 시간은 231-1보다 작거나 같은 자연수 또는 0이다.

{{< output >}}
출력
{{< /output >}}

첫째 줄에 최대 사용할 수 있는 회의의 최대 개수를 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
5
4 4
4 4
3 4
2 4
1 4
//출력1
3
//입력2
11
1 4
4 4
0 6
5 7
3 8
5 9
6 10
8 11
8 12
2 13
12 14
//출력2
4
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const input = require('fs').readFileSync('/dev/stdin')
const allData = input
  .toString()
  .trim()
  .split(/\n+/)
  .map((i) =>
    i
      .toString()
      .trim()
      .split(/\s+/)
      .map((n) => +n)
  )
const [N] = allData[0]
const data = allData.slice(1)

const main = () => {
  const sorted = data.sort((pre, post) => pre[1] - post[1] || pre[0] - post[0])
  let count = 0
  let deadline = 0
  sorted.forEach((item) => {
    if (deadline <= item[0]) {
      deadline = item[1]
      count++
    }
  })
  console.log(count)
}

main()
{{< /highlight >}}
