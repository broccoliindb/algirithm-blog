---
title: "Coin"
displayTitle: "동전"
date: 2020-11-05T14:20:13+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "첫째 줄에 K원을 만드는데 필요한 동전 개수의 최솟값을 출력한다."
tags: []
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "84ab6cd5692e76fec107b96319591a4f"
disqus_title: "Coin"
disqus_url: "84ab6cd5692e76fec107b96319591a4f"
---

## 1,2,3 더하기

{{< problem >}}
문제
{{< /problem >}}

준규가 가지고 있는 동전은 총 N종류이고, 각각의 동전을 매우 많이 가지고 있다.

동전을 적절히 사용해서 그 가치의 합을 K로 만들려고 한다. 이때 필요한 동전 개수의 최솟값을 구하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 N과 K가 주어진다. (1 ≤ N ≤ 10, 1 ≤ K ≤ 100,000,000)

둘째 줄부터 N개의 줄에 동전의 가치 Ai가 오름차순으로 주어진다. (1 ≤ Ai ≤ 1,000,000, A1 = 1, i ≥ 2인 경우에 Ai는 Ai-1의 배수)

{{< output >}}
출력
{{< /output >}}

첫째 줄에 K원을 만드는데 필요한 동전 개수의 최솟값을 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
10 4200
1
5
10
50
100
500
1000
5000
10000
50000
//출력1
6
//입력1
10 4790
1
5
10
50
100
500
1000
5000
10000
50000
//출력1
12
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const input = require("fs").readFileSync("/dev/stdin");
const data = input.toString().trim().split(/\n+/);
const [N, K] = data[0].split(/\s+/).map((n) => +n);
const info = data.slice(1).map((n) => +n);

let answer = 0;
let rest = K;
let share = 0;
for (let i = N - 1; i >= 0; i--) {
  share = Math.floor(rest / info[i]);
  if (share > 0) {
    rest = rest % info[i];
    answer += share;
  }
  if (i === 0 && rest > 0) {
    answer++;
  }
}
console.log(answer);
{{< /highlight >}}
