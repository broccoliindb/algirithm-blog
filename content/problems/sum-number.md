---
title: "Sum Number"
displayTitle: "숫자의 합"
date: 2020-11-20T09:46:54+09:00
isCJKLanguage: true
draft: false
categories: []
levels: [1]
description: "입력으로 주어진 숫자 N개의 합을 출력한다."
tags: [string]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "9692b0f601e0a8982c2183e95a6bc7d3"
disqus_title: "Sum Number"
disqus_url: "9692b0f601e0a8982c2183e95a6bc7d3"
---

## 숫자의 합

{{< problem >}}
문제
{{< /problem >}}

N개의 숫자가 공백 없이 쓰여있다. 이 숫자를 모두 합해서 출력하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 숫자의 개수 N (1 ≤ N ≤ 100)이 주어진다. 둘째 줄에 숫자 N개가 공백없이 주어진다.

{{< output >}}
출력
{{< /output >}}

입력으로 주어진 숫자 N개의 합을 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
// 예제 입력 1 
1
1
// 예제 출력 1 
1
// 예제 입력 2 
5
54321
// 예제 출력 2 
15
// 예제 입력 3 
25
7000000000000000000000000
// 예제 출력 3 
7
// 예제 입력 4 
11
10987654321
// 예제 출력 4 
46
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const input = require('fs')
  .readFileSync('/dev/stdin')
  .toString()
  .trim()
  .split(/\n+/)
const N = parseInt(input[0], 10)
const data = input[1]
  .toString()
  .trim()
  .split('')
  .map((n) => +n)
const main = () => {
  const answer = data.reduce((acc, cur) => acc + cur)
  console.log(answer)
}
main()
{{< /highlight >}}
