---
title: "Search Route"
displayTitle: "루트찾기"
date: 2020-10-05T00:09:01+09:00
isCJKLanguage: true
draft: false
categories: [floyd-warshall]
levels: []
description: "플로이드 와샬을 이용한 루트찾기"
tags: []
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "80d5f23ab8d8cac2c778b45731899542"
disqus_title: "Search Route"
disqus_url: "80d5f23ab8d8cac2c778b45731899542"
---
## 경로찾기

{{< problem >}}
문제
{{< /problem >}}

가중치 없는 방향 그래프 G가 주어졌을 때, 모든 정점 (i, j)에 대해서, i에서 j로 가는 경로가 있는지 없는지 구하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 정점의 개수 N (1 ≤ N ≤ 100)이 주어진다. 둘째 줄부터 N개 줄에는 그래프의 인접 행렬이 주어진다. i번째 줄의 j번째 숫자가 1인 경우에는 i에서 j로 가는 간선이 존재한다는 뜻이고, 0인 경우는 없다는 뜻이다. i번째 줄의 i번째 숫자는 항상 0이다.

{{< output >}}
출력
{{< /output >}}

총 N개의 줄에 걸쳐서 문제의 정답을 인접행렬 형식으로 출력한다. 정점 i에서 j로 가는 경로가 있으면 i번째 줄의 j번째 숫자를 1로, 없으면 0으로 출력해야 한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
3
0 1 0
0 0 1
1 0 0
//출력1
1 1 1
1 1 1
1 1 1
//입력2
7
0 0 0 1 0 0 0
0 0 0 0 0 0 1
0 0 0 0 0 0 0
0 0 0 0 1 1 0
1 0 0 0 0 0 0
0 0 0 0 0 0 1
0 0 1 0 0 0 0
//출력2
1 0 1 1 1 1 1
0 0 1 0 0 0 1
0 0 0 0 0 0 0
1 0 1 1 1 1 1
1 0 1 1 1 1 1
0 0 1 0 0 0 1
0 0 1 0 0 0 0
```

{{< solution >}}
#1. 풀이확인 js
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

const getResult = (data) => {

  for (let k = 0; k < N; k++) {
    for (let i = 0; i < N; i++) {
      for (let j = 0; j < N; j++) {
        if(data[i][k] > 0 && data[k][j] > 0 && data[i][j]=== 0) {
          data[i][j] = 1
        }
      }
    }
  }
  for(let i=0; i<data.length; i++) {
    console.log(data[i].join(' '))
  }
}

let input = require('fs').readFileSync('/dev/stdin')
const data = input.toString().trim().split(/\n+/).map(i => i.trim().split(/\s+/).map(n => +n))
const N = data[0]

getResult(data.slice(1))

{{< /highlight >}}
