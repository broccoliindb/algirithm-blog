---
title: "Add One Two Three"
displayTitle: "1,2,3 더하기"
date: 2020-10-31T22:52:42+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "각 테스트 케이스마다, n을 1, 2, 3의 합으로 나타내는 방법의 수를 출력한다."
tags: [memoization, dynamic]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "bffabf6992b855abfcc019d3e8dfb9d5"
disqus_title: "Add One Two Three"
disqus_url: "bffabf6992b855abfcc019d3e8dfb9d5"
---

## 1,2,3 더하기

{{< problem >}}
문제
{{< /problem >}}

정수 4를 1, 2, 3의 합으로 나타내는 방법은 총 7가지가 있다. 합을 나타낼 때는 수를 1개 이상 사용해야 한다.

```
1+1+1+1
1+1+2
1+2+1
2+1+1
2+2
1+3
3+1
```

정수 n이 주어졌을 때, n을 1, 2, 3의 합으로 나타내는 방법의 수를 구하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 테스트 케이스의 개수 T가 주어진다. 각 테스트 케이스는 한 줄로 이루어져 있고, 정수 n이 주어진다. n은 양수이며 11보다 작다.

{{< output >}}
출력
{{< /output >}}

각 테스트 케이스마다, n을 1, 2, 3의 합으로 나타내는 방법의 수를 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
3
4
7
10
//출력1
7
44
274
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
let data = require('fs').readFileSync('/dev/stdin').toString().trim().split(/\n+/).map(i => parseInt(i.toString().trim(), 10))
const cases = data[0]
const info = data.slice(1)

const T = (N, save) => {
  if (N === 1) {
    save[N] = 1
    return save[N]
  }
  if (N === 2) {
    save[N] = 2
    return save[N]
  }
  if (N === 3) {
    save[N] = 4
    return save[N]
  }
  if (N >= 4) {
     return (save[N-1] || T(N-1, save)) + (save[N-2] || T(N-2, save)) + (save[N-3] || T(N-3, save))
  }
}

const getResult = () => {
  const save = {}
  for (let i = 0; i < cases; i++) {
    console.log(T(info[i], save))
  }
}

getResult()
{{< /highlight >}}
