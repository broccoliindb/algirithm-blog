---
title: "Moon Travel"
displayTitle: "진우의 달 여행(Large)"
date: 2021-03-24T02:59:15+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "최대한 돈을 아끼고 살아서 달에 도착하고 싶은 진우를 위해 달에 도달하기 위해 필요한 연료의 최소값을 계산해 주자."
tags: [dynamic-programming]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "75b12328394401d9cdcd2290ff41b630"
disqus_title: "Moon Travel Large"
disqus_url: "75b12328394401d9cdcd2290ff41b630"
---
## 진우의 달 여행 (Large)

{{< problem >}}
문제
{{< /problem >}}

우주비행이 꿈이였던 진우는 음식점 '매일매일싱싱'에서 열심히 일한 결과 달 여행에 필요한 자금을 모두 마련하였다! 

지구와 우주사이는 N X M 행렬로 나타낼 수 있으며 각 원소의 값은 우주선이 그 공간을 지날 때 소모되는 연료의 양이다.

![moon-travel-large1](/problems/images/moon-travel-large1.png)

진우는 여행경비를 아끼기 위해 조금 특이한 우주선을 선택하였다. 진우가 선택한 우주선의 특징은 아래와 같다.

1.지구 -> 달로 가는 경우 우주선이 움직일 수 있는 방향은 아래와 같다.

![moon-travel-large2](/problems/images/moon-travel-large2.png)

2.우주선은 전에 움직인 방향으로 움직일 수 없다. 
즉, 같은 방향으로 두번 연속으로 움직일 수 없다.

진우의 목표는 연료를 최대한 아끼며 지구의 어느위치에서든 출발하여 달의 어느위치든 착륙하는 것이다.

최대한 돈을 아끼고 살아서 달에 도착하고 싶은 진우를 위해 달에 도달하기 위해 필요한 연료의 최소값을 계산해 주자.

{{< input >}}
입력
{{< /input >}}

첫줄에 지구와 달 사이 공간을 나타내는 행렬의 크기를 나타내는 N, M (2 ≤ N, M ≤ 1000)이 주어진다.
다음 N줄 동안 각 행렬의 원소 값이 주어진다. 각 행렬의 원소값은 100 이하의 자연수이다.

```
6 4
5 8 5 1
3 5 8 4
9 77 65 5
2 1 5 2
5 98 1 5
4 95 67 58
```


{{< output >}}
출력
{{< /output >}}

달 여행에 필요한 최소 연료의 값을 출력한다.

```
29
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
/**
 * 0: 아래방향
 * 1: 우아래방향
 * 2: 좌아래방향
 */

const input = require('fs').readFileSync('/dev/stdin')
const info = input
  .toString()
  .trim()
  .split(/\n+/)
  .map((i) =>
    i
      .toString()
      .trim()
      .split(/\s+/)
      .map((n) => +n)
  )
const [N, M] = info[0]
const data = info.slice(1)
const dp = Array.from({ length: N }).map((i) =>
  Array.from({ length: M }).map((j) => Array.from({ length: 3 }))
)

for (let x = 0; x < M; x++) {
  for (let k = 0; k < 3; k++) {
    dp[0][x][k] = data[0][x]
  }
}
for (let y = 1; y < N; y++) {
  for (let x = 0; x < M; x++) {
    //좌측 끝
    if (x === 0) {
      dp[y][x][0] = dp[y - 1][x + 1][2] + data[y][x]
      dp[y][x][1] = Math.min(dp[y - 1][x][0], dp[y - 1][x + 1][2]) + data[y][x]
      //우측끝
    } else if (x === M - 1) {
      dp[y][x][0] = dp[y - 1][x - 1][1] + data[y][x]
      dp[y][x][2] = Math.min(dp[y - 1][x][0], dp[y - 1][x - 1][1]) + data[y][x]
      //사이
    } else {
      dp[y][x][0] =
        Math.min(dp[y - 1][x - 1][1], dp[y - 1][x + 1][2]) + data[y][x]
      dp[y][x][1] = Math.min(dp[y - 1][x][0], dp[y - 1][x + 1][2]) + data[y][x]
      dp[y][x][2] = Math.min(dp[y - 1][x][0], dp[y - 1][x - 1][1]) + data[y][x]
    }
  }
}
let min = Infinity
for (let x = 0; x < M; x++) {
  for (let k = 0; k < 3; k++) {
    if (!dp[N - 1][x][k]) continue
    if (dp[N - 1][x][k] < min) {
      min = dp[N - 1][x][k]
    }
  }
}
console.log(min)

{{< /highlight >}}