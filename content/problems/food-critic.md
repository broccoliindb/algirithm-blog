---
title: "Food Critic"
displayTitle: "음식 평론가"
date: 2020-09-03T23:12:26+09:00
isCJKLanguage: true
draft: false
categories: [math]
levels: ['level2']
description: "모든 평론가에게 같은 양의 소시지를 주기위해 필요한 칼질의 수 구하기"
tags: []
solutions: ['completed']
authors: [broccoliindb]
weight: 0
disqus_identifier: "68d4463eabcaa3c94bf0c8506a7593ae"
disqus_title: "Food Critic"
disqus_url: "68d4463eabcaa3c94bf0c8506a7593ae"
---

## 음식 평론가

{{< problem >}}
문제
{{< /problem >}}


선영이의 직업은 소시지 요리사이다. 소시지를 팔기 전에 음식 평론가 M명을 모아서 맛을 테스트해보려고 한다.

선영이는 동일한 소시지를 총 N개를 준비했다. 이 소시지를 모든 평론가들이 같은 양을 받게 소시지를 자르려고 한다. 이때, 소시지를 자르는 횟수를 최소로 하려고 한다.

예를 들어, 소시지가 2개, 평론가가 6명있는 경우를 생각해보자. 이때, 각 소시지를 세 조각으로 만든 다음, 각 평론가에게 한 조각씩 주면 된다. 이 경우에 소시지는 총 네 번 자르게 된다. 다른 경우로 소시지가 3개, 평론가가 4명 있는 경우를 생각해보자. 이때는 각 소시지의 크기를 3:1로 잘라서 큰 조각을 평론가에게 하나씩 주고, 남은 조각을 평론가에게 주면 모두 동일한 양을 받게 된다.

소시지의 수와 평론가의 수가 주어졌을 때, 모든 평론가에게 같은 양의 소시지를 주기 위해 필요한 칼질의 수를 구하는 프로그램을 작성하시오. 

{{< input >}}
입력
{{< /input >}}
첫째 줄에 소시지의 수 N과 평론가의 수 M이 주어진다. (1 ≤ N, M ≤ 100)

```
2 6
```


{{< output >}}
출력
{{< /output >}}
첫째 줄에 모든 평론가에게 동일한 양을 주기 위해 필요한 칼질 횟수의 최솟값을 출력한다. 
```
4
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
1<=n, m <= 100
m: 평론가 수
n: 빵수

1) n >= m : 빵이 평론가보다 많을 때

빵조각수: Math.ceil(m/n)
칼질수: (빵조각수 - 1)*n

2) m > n : 평론가가 빵보다 많을때

빵조각수 : Math.ceil(m/(m - n))
칼질수: (빵조각수 - 1)*(m - n)

const getBreadCutCount = (m, n) => {
  let cutCount = -1
  let breadNum = -1
  if (n >= m) {
    breadNum = Math.ceil(m/n)
    cutCount = (breadNum - 1) * n
  } else {
    breadNum = Math.ceil(m/(n - m))
    cutCount = (breadNum - 1) * (n - m)
  }
  return cutCount
}
{{< /highlight >}}