---
title: "Star"
displayTitle: "별 찍기"
date: 2020-11-20T09:46:38+09:00
isCJKLanguage: true
draft: false
categories: []
levels: [1]
description: "첫째 줄에는 별 1개, 둘째 줄에는 별 2개, N번째 줄에는 별 N개를 찍는 문제"
tags: ["string"]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "366d41af80e31637be74affd8d8affe9"
disqus_title: "Star"
disqus_url: "366d41af80e31637be74affd8d8affe9"
---

## 별 찍기

{{< problem >}}
문제
{{< /problem >}}

첫째 줄에는 별 1개, 둘째 줄에는 별 2개, N번째 줄에는 별 N개를 찍는 문제

{{< input >}}
입력
{{< /input >}}

첫째 줄에 N(1 ≤ N ≤ 100)이 주어진다.

{{< output >}}
출력
{{< /output >}}

첫째 줄부터 N번째 줄까지 차례대로 별을 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
// 예제 입력 1 
5
// 예제 출력 1 
*
**
***
****
*****
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const N = parseInt(
  require('fs').readFileSync('/dev/stdin').toString().trim(),
  10
)
const main = (N) => {
  let star = ''
  for (let i = 0; i < N; i++) {
    star += '*'
    console.log(star)
  }
}
main(N)
{{< /highlight >}}
