---
title: "Witches With Fireball"
displayTitle: "마법사 상어와 파이어볼"
date: 2020-10-27T11:56:38+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "마법사 상어가 이동을 K번 명령한 후, 남아있는 파이어볼 질량의 합을 출력한다."
tags: [math, simulation]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "dedc7f04fd2c958677200182bdc5fe79"
disqus_title: "Witches With Fireball"
disqus_url: "dedc7f04fd2c958677200182bdc5fe79"
---

## 마법사 상어와 파이어볼

{{< problem >}}
문제
{{< /problem >}}

어른 상어가 마법사가 되었고, 파이어볼을 배웠다.

마법사 상어가 크기가 N×N인 격자에 파이어볼 M개를 발사했다. 가장 처음에 파이어볼은 각자 위치에서 이동을 대기하고 있다. i번 파이어볼의 위치는 (ri, ci), 질량은 mi이고, 방향은 di, 속력은 si이다. 위치 (r, c)는 r행 c열을 의미한다.

격자의 행과 열은 1번부터 N번까지 번호가 매겨져 있고, 1번 행은 N번과 연결되어 있고, 1번 열은 N번 열과 연결되어 있다.

파이어볼의 방향은 어떤 칸과 인접한 8개의 칸의 방향을 의미하며, 정수로는 다음과 같다.

![directionForFireball](/problems/images/witches-with-fireball.png)

마법사 상어가 모든 파이어볼에게 이동을 명령하면 다음이 일들이 일어난다.

모든 파이어볼이 자신의 방향 di로 속력 si칸 만큼 이동한다.
이동하는 중에는 같은 칸에 여러 개의 파이어볼이 있을 수도 있다.
이동이 모두 끝난 뒤, 2개 이상의 파이어볼이 있는 칸에서는 다음과 같은 일이 일어난다.
같은 칸에 있는 파이어볼은 모두 하나로 합쳐진다.
파이어볼은 4개의 파이어볼로 나누어진다.
나누어진 파이어볼의 질량, 속력, 방향은 다음과 같다.
질량은 ⌊(합쳐진 파이어볼 질량의 합)/5⌋이다.
속력은 ⌊(합쳐진 파이어볼 속력의 합)/(합쳐진 파이어볼의 개수)⌋이다.
합쳐지는 파이어볼의 방향이 모두 홀수이거나 모두 짝수이면, 방향은 0, 2, 4, 6이 되고, 그렇지 않으면 1, 3, 5, 7이 된다.
질량이 0인 파이어볼은 소멸되어 없어진다.
마법사 상어가 이동을 K번 명령한 후, 남아있는 파이어볼 질량의 합을 구해보자.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 N, M, K가 주어진다.

둘째 줄부터 M개의 줄에 파이어볼의 정보가 한 줄에 하나씩 주어진다. 파이어볼의 정보는 다섯 정수 ri, ci, mi, si, di로 이루어져 있다.

서로 다른 두 파이어볼의 위치가 같은 경우는 입력으로 주어지지 않는다.


{{< output >}}
출력
{{< /output >}}

마법사 상어가 이동을 K번 명령한 후, 남아있는 파이어볼 질량의 합을 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
4 2 1
1 1 5 2 2
1 4 7 1 6
//출력1
8
//입력2
4 2 2
1 1 5 2 2
1 4 7 1 6
//출력2
8
//입력3
4 2 3
1 1 5 2 2
1 4 7 1 6
//출력3
0
//입력4
7 5 3
1 3 5 2 4
2 3 5 2 6
5 2 9 1 7
6 2 1 3 5
4 4 2 4 2
//출력4
9
```

{{< solution >}}
#1. 풀이확인 js 
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

let input = require('fs').readFileSync('/dev/stdin')
let data = input.toString().trim().split(/\n+/).map(i => i.toString().trim().split(/\s+/).map(n => +n))

const [N, M, K] = data[0]
const fireballs = data.slice(1)

const getInitialMap = (fireballs) => {
  const initialMap = new Map()
  for (let i = 0; i < M; i++) {
    const [y, x, m, s, d] = fireballs[i]
    if (!initialMap.has(`${x},${y}`)) {
      initialMap.set(`${x},${y}`, [[y, x, m, s, d]])
    } else {
      initialMap.get(`${x},${y}`).push([y, x, m, s, d])
    }
  }
  return initialMap
}

const regenerateMap = (movedMap) => {
  const regenerated = new Map()
  for (const [key, value] of movedMap) {
    let sumM = 0
    let sumS = 0
    let allEvenOrOdd = false
    let evenCount = 0
    if (movedMap.get(key).length > 1) {
      let y, x, m, s, d
      movedMap.get(key).forEach((fireball) => {
        [y, x, m, s, d] = fireball
        sumM += m
        sumS += s
        if (d % 2 === 0) {
          evenCount++
        }
      })
      if (evenCount === movedMap.get(key).length || evenCount === 0) {
        allEvenOrOdd = true
      } else {
        allEvenOrOdd = false
      }
      const postM = Math.floor(sumM / 5)
      const postS = Math.floor(sumS / movedMap.get(key).length)
      if (postM !== 0) {
        regenerated.set(key, [])
        if (allEvenOrOdd) {
          for (let i = 0; i < 4; i++) {
            regenerated.get(key).push([y, x, postM, postS, 2 * i])
          }
        } else {
          for (let i = 0; i < 4; i++) {
            regenerated.get(key).push([y, x, postM, postS, 2 * i + 1])
          }
        }
      }
    } else {
      regenerated.set(key, value)
    }
  }
  return regenerated
}

const moveFireBalls = (initialMap) => {
  const ny = [-1, -1, 0, 1, 1, 1, 0, -1]
  const nx = [0, 1, 1, 1, 0, -1, -1, -1]
  const newFireballMap = new Map()
  for (const [key, value] of initialMap) {
    initialMap.get(key).forEach(fireball => {
      const [y, x, m, s, d] = fireball
      let dy = (y + ny[d] * s) % N
      if (dy <= 0) {
        dy = dy + N
      }
      let dx = (x + nx[d] * s) % N
      if (dx <= 0) {
        dx = dx + N
      }
      if (!newFireballMap.has(`${dx},${dy}`)) {
        newFireballMap.set(`${dx},${dy}`, [])
      }
      newFireballMap.get(`${dx},${dy}`).push([dy, dx, m, s, d])
    })
  }
  return newFireballMap
}

const getSumWeight = (fireballsMap) => {
  let sumM = 0
  for (const [key, value] of fireballsMap) {
    fireballsMap.get(key).forEach(value => {
      sumM += value[2]
    })
  }
  console.log(sumM)
}

const init = () => {
  const initialMap = getInitialMap(fireballs)
  let regenerated = regenerateMap(initialMap)
  let moved = null
  for (let i = 0; i < K; i++) {
    moved = moveFireBalls(regenerated)
    regenerated = regenerateMap(moved)
  }
  getSumWeight(regenerated)
}

init()

{{< /highlight >}}

이번문제는 테스트예제를 통과했지만 통과가 안되서 2틀을 잡아먹었다..... 문제는 전체가 홀수와 전체가 짝수인경우를 판별하는 조건을 잘못해서였음....

단순히 합쳐서 짝수면 전체가 홀수나 짝수일거라 단순히 생각했던게 문제였다....

통과는했지만 메모리가 좀 높았다.ㅠ