---
title: "Friend Network"
displayTitle: "친구 네트워크"
date: 2020-12-04T14:32:01+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "친구 관계가 생길 때마다, 두 사람의 친구 네트워크에 몇 명이 있는지 구하는 프로그램을 작성하시오."
tags: ["union find"]
solutions: [uncompleted]
authors: [broccoliindb]
weight: 0
disqus_identifier: "20c088a0efe856566ccd86cbd91b54b6"
disqus_title: "Friend Network"
disqus_url: "20c088a0efe856566ccd86cbd91b54b6"
---

## 친구 네트워크

{{< problem >}}
문제
{{< /problem >}}

민혁이는 소셜 네트워크 사이트에서 친구를 만드는 것을 좋아하는 친구이다. 우표를 모으는 취미가 있듯이, 민혁이는 소셜 네트워크 사이트에서 친구를 모으는 것이 취미이다.

어떤 사이트의 친구 관계가 생긴 순서대로 주어졌을 때, 두 사람의 친구 네트워크에 몇 명이 있는지 구하는 프로그램을 작성하시오.

친구 네트워크란 친구 관계만으로 이동할 수 있는 사이를 말한다.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 테스트 케이스의 개수가 주어진다. 각 테스트 케이스의 첫째 줄에는 친구 관계의 수 F가 주어지며, 이 값은 100,000을 넘지 않는다. 다음 F개의 줄에는 친구 관계가 생긴 순서대로 주어진다. 친구 관계는 두 사용자의 아이디로 이루어져 있으며, 알파벳 대문자 또는 소문자로만 이루어진 길이 20 이하의 문자열이다.
{{< output >}}
출력
{{< /output >}}

친구 관계가 생길 때마다, 두 사람의 친구 네트워크에 몇 명이 있는지 구하는 프로그램을 작성하시오.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//예제 입력 1 
2
3
Fred Barney
Barney Betty
Betty Wilma
3
Fred Barney
Betty Wilma
Barney Betty
//예제 출력 1 
2
3
4
2
2
4
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const input = require('fs').readFileSync('/dev/stdin')
const info = input
  .toString()
  .trim()
  .split(/\n+/)
  .map((i) =>
    i
      .toString()
      .trim()
      .split(/\s+/)
      .map((n) => n)
  )
const data = info

class disjoinSet {
  constructor() {
    this.setList = {}
  }

  makeSet(element) {
    const node = {
      self: element,
      parent: element,
      count: 1
    }
    this.setList[element] = node
  }

  union(node1, node2) {
    const root1 = this.find(node1)
    const root2 = this.find(node2)
    if (root1 !== root2) {
      if (root1.count > root2.count) {
        root2.parent = root1
        root1.count = root1.count + root2.count
        console.log(root1.count)
      } else {
        root1.parent = root2
        root2.count = root2.count + root1.count
        console.log(root2.count)
      }
    } else {
      console.log(root1.count)
    }
  }

  find(node) {
    if (node.self === node.parent) {
      return node
    } else {
      node.parent = this.find(node.parent)
      return node.parent
    }
  }
}

const main = () => {
  const parent = []
  const [TEST_CASE] = data[0]

  let start = 2
  for (let j = 0; j < TEST_CASE; j++) {
    let set = new disjoinSet()
    const { setList } = set
    let testSize = parseInt(data[1], 10)
    let end = start + testSize
    for (let i = start; i < end; i++) {
      const [a, b] = data[i]
      if (!setList[a]) {
        set.makeSet(a)
      }
      if (!setList[b]) {
        set.makeSet(b)
      }

      set.union(setList[a], setList[b])
    }
    testSize = end
    start = testSize + 1
    end = start + testSize
  }
}

main()

{{< /highlight >}}