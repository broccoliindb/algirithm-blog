---
title: "Search Number"
displayTitle: "수 찾기"
date: 2020-10-05T00:08:47+09:00
isCJKLanguage: true
draft: false
categories: [binary-search]
levels: []
description: "이분탐색을 이용한 수찾기"
tags: []
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "1d533bc3f12c4fb1f1b745b7e8021bee"
disqus_title: "Search Number"
disqus_url: "1d533bc3f12c4fb1f1b745b7e8021bee"
---
## 수 찾기

{{< problem >}}
문제
{{< /problem >}}

N개의 정수 A[1], A[2], …, A[N]이 주어져 있을 때, 이 안에 X라는 정수가 존재하는지 알아내는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 자연수 N(1≤N≤100,000)이 주어진다. 다음 줄에는 N개의 정수 A[1], A[2], …, A[N]이 주어진다. 다음 줄에는 M(1≤M≤100,000)이 주어진다. 다음 줄에는 M개의 수들이 주어지는데, 이 수들이 A안에 존재하는지 알아내면 된다. 모든 정수의 범위는 -231 보다 크거나 같고 231보다 작다.

```js
5
4 1 5 2 3
5
1 3 7 9 5
```

{{< output >}}
출력
{{< /output >}}

M개의 줄에 답을 출력한다. 존재하면 1을, 존재하지 않으면 0을 출력한다.

```js
1
1
0
0
1
```

{{< solution >}}
#1. 풀이확인 js
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

const input = require('fs').readFileSync('/dev/stdin')
const data = input.toString().trim().split(/\n+/)
const A = data[1].toString().trim().split(/\s+/).map(i => +i).sort((a, b) => a - b)
const targetArr = data[3].toString().trim().split(/\s+/).map(i => +i)
const searchTarget = (target, s, e) => {
  let mid = Math.floor((s + e) / 2)
  if (s <= e) {
    if (target === A[mid]) {
      return 1
    } else if (target > A[mid]) {
      return searchTarget(target, ++mid, e)
    } else {
      return searchTarget(target, s, --mid)
    }
  }
  return 0
}

for (let i = 0; i < targetArr.length; i++) {
  console.log(searchTarget(targetArr[i], 0, targetArr.length - 1))
}

{{< /highlight >}}

