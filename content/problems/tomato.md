---
title: "Tomato"
displayTitle: "토마토"
date: 2020-10-20T14:09:06+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "며칠이 지나면 토마토들이 모두 익는지, 그 최소 일수를 구하는 프로그램을 작성하라"
tags: [bfs, shift]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "bcc4017105b976977acfdce576835182"
disqus_title: "Tomato"
disqus_url: "bcc4017105b976977acfdce576835182"
---

## 토마토

{{< problem >}}
문제
{{< /problem >}}

철수의 토마토 농장에서는 토마토를 보관하는 큰 창고를 가지고 있다. 토마토는 아래의 그림과 같이 격자 모양 상자의 칸에 하나씩 넣어서 창고에 보관한다.

창고에 보관되는 토마토들 중에는 잘 익은 것도 있지만, 아직 익지 않은 토마토들도 있을 수 있다. 보관 후 하루가 지나면, 익은 토마토들의 인접한 곳에 있는 익지 않은 토마토들은 익은 토마토의 영향을 받아 익게 된다. 하나의 토마토의 인접한 곳은 왼쪽, 오른쪽, 앞, 뒤 네 방향에 있는 토마토를 의미한다. 대각선 방향에 있는 토마토들에게는 영향을 주지 못하며, 토마토가 혼자 저절로 익는 경우는 없다고 가정한다. 철수는 창고에 보관된 토마토들이 며칠이 지나면 다 익게 되는지, 그 최소 일수를 알고 싶어 한다.

토마토를 창고에 보관하는 격자모양의 상자들의 크기와 익은 토마토들과 익지 않은 토마토들의 정보가 주어졌을 때, 며칠이 지나면 토마토들이 모두 익는지, 그 최소 일수를 구하는 프로그램을 작성하라. 단, 상자의 일부 칸에는 토마토가 들어있지 않을 수도 있다.

{{< input >}}
입력
{{< /input >}}

첫 줄에는 상자의 크기를 나타내는 두 정수 M,N이 주어진다. M은 상자의 가로 칸의 수, N은 상자의 세로 칸의 수를 나타낸다. 단, 2 ≤ M,N ≤ 1,000 이다. 둘째 줄부터는 하나의 상자에 저장된 토마토들의 정보가 주어진다. 즉, 둘째 줄부터 N개의 줄에는 상자에 담긴 토마토의 정보가 주어진다. 하나의 줄에는 상자 가로줄에 들어있는 토마토의 상태가 M개의 정수로 주어진다. 정수 1은 익은 토마토, 정수 0은 익지 않은 토마토, 정수 -1은 토마토가 들어있지 않은 칸을 나타낸다.


{{< output >}}
출력
{{< /output >}}

여러분은 토마토가 모두 익을 때까지의 최소 날짜를 출력해야 한다. 만약, 저장될 때부터 모든 토마토가 익어있는 상태이면 0을 출력해야 하고, 토마토가 모두 익지는 못하는 상황이면 -1을 출력해야 한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
6 4
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 1
//출력1
8
//입력2
6 4
0 -1 0 0 0 0
-1 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 1
//출력2
-1
//입력3
6 4
1 -1 0 0 0 0
0 -1 0 0 0 0
0 0 0 0 -1 0
0 0 0 0 -1 1
//출력3
6
//입력4
5 5
-1 1 0 0 0
0 -1 -1 -1 0
0 -1 -1 -1 0
0 -1 -1 -1 0
0 0 0 0 0
//출력4
14
//입력5
2 2
1 -1
-1 1
//출력5
0
```

{{< solution >}}
#1. 풀이확인 js 
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

let input = require('fs').readFileSync('/dev/stdin')
let data = input.toString().trim().split(/\n+/)
const [M, N] = data[0].toString().trim().split(/\s+/).map(n => +n)
const info = data.slice(1).map(i => i.toString().trim().split(',')).map(n => n.toString().trim().split(/\s+/).map(n => +n))

const getStarts = (info) => {
  const starts = []
  let cnt = 0
  for (let i = 0; i < N; i++) {
    for (let j = 0; j < M; j++) {
      if (info[i][j] === 1) {
        starts.push([i, j])
      } else if (info[i][j] === 0) {
        cnt++
      }
    }
  }
  if (cnt === 0) {
    starts.length = 0
  }
  return starts
}

const bfs = (queue) => {
  const nx = [0, 0, -1, 1]
  const ny = [-1, 1, 0, 0]
  let max = 0
  let s = 0
  while (queue[s]) {
    let [x, y] = queue[s]
    //queue.shift()를 사용했다가 사용안함 : 시간초과
    for (let k = 0; k < 4; k++) {
      const dx = x + nx[k]
      const dy = y + ny[k]
      if (dx >= 0 && dx < N && dy >= 0 && dy < M && info[dx][dy] === 0) {
        info[dx][dy] = info[x][y] + 1
        max = max >= info[dx][dy] ? max : info[dx][dy]
        queue.push([dx, dy])
      }
    }
    s++
  }
  return max
}

const main = () => {
  const starts = getStarts(info)
  if (starts.length === 0) {
    console.log(0)
    return
  }
  const max = bfs(starts)
  if (info.toString().trim().split(',').includes('0')) {
    console.log(-1)
    return
  }
  console.log(max - 1)
}

main()

{{< /highlight >}}

이번문제는 javascript의 shift가 얼마나 느린지 간과한것에서 문제가 시작됐다... 알고리즘 구조는 진작에 맞았지만, shift가 이렇게 느릴줄이야....ㅠ