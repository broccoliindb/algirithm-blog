---
title: "Sum Number Set2"
displayTitle: "수들의 합2"
date: 2020-09-29T11:18:11+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "특정 index에서 특정 index까지의 합이 조건을 만족하는 경우찾기"
tags: ["two pointer"]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "7f723d4840f2c1608f585e1870d0a9bb"
disqus_title: "Sum Number Set2"
disqus_url: "7f723d4840f2c1608f585e1870d0a9bb"
---

## 수들의 합2

{{< problem >}}
문제
{{< /problem >}}

N개의 수로 된 수열 A[1], A[2], …, A[N] 이 있다. 이 수열의 i번째 수부터 j번째 수까지의 합 A[i]+A[i+1]+…+A[j-1]+A[j]가 M이 되는 경우의 수를 구하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 N(1≤N≤10,000), M(1≤M≤300,000,000)이 주어진다. 다음 줄에는 A[1], A[2], …, A[N]이 공백으로 분리되어 주어진다. 각각의 A[x]는 30,000을 넘지 않는 자연수이다.

```js
//입력예1
4 2
1 1 1 1
```

```js
//입력예2
10 5
1 2 3 4 2 5 3 1 1 2
```

{{< output >}}
출력
{{< /output >}}

첫째 줄에 경우의 수를 출력한다.

```js
//출력예1
3
```

```js
//출력예2
3
```

{{< solution >}}
#1. 풀이확인 js
{{< /solution >}}

{{< highlight "js" "linenos=table,hl_lines='',linenostart=1" >}}

const readline = require('readline')
const r = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

let input = []
r.on('line', (line) => {
  input.push(line)
  if (input.length === 2) {
    const N = parseInt(input[0].split(' ')[0], 10)
    const M = parseInt(input[0].split(' ')[1], 10)
    const Arr = input[1].split(' ').map(i => parseInt(i, 10))
  
    let start = 0
    let end = 0
    let sum = Arr[end]
    let count = 0
    while (end < Arr.length) {
      if (sum === M) {
        // console.log(start, end, sum, M)
        count++
        end++
        sum = sum + Arr[end] - Arr[start]
        start++
      } else if (sum < M) {
        // console.log(start, end, sum, M)
        end++
        sum += Arr[end]
      } else {
        // console.log(start, end, sum, M)
        sum -= Arr[start]
        start++
      }
    }
    console.log(count)
    input.length = 0
  }
  
}).on('close', () => {

})

{{< /highlight >}}
