---
title: "Serverroom"
displayTitle: "서버실"
date: 2020-09-04T21:34:19+09:00
isCJKLanguage: true
draft: false
categories: ["Binary Search"]
levels: [level3]
description: "컴퓨터의 절반 이상이 동작하는 최소 분수 구하기"
tags: ["binary search"]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "184b4eb3e7ea85a55bc15125eb4f4fcc"
disqus_title: "Serverroom"
disqus_url: "184b4eb3e7ea85a55bc15125eb4f4fcc"
---
## 서버실

{{< problem >}}
문제
{{< /problem >}}


서버실은 여러 대의 서버 컴퓨터들을 안정적으로 운영할 수 있는 환경을 유지하기 위해 설치된 공간을 말한다.

이 회사의 서버실은 N×N 칸으로 구분되어 있고, 각 칸마다 서버 랙이 있어 컴퓨터를 여러 대 쌓을 수 있다. 서버가 과열되지 않도록 서버실에는 언제나 냉방기가 작동하고 있다. 그런데 회사가 경제적으로 어려움에 처한 나머지, 서버실의 운영 비용을 줄이기 위해 서버실 내의 컴퓨터 중 절반만 정상적으로 관리하기로 하였다.

냉방기에서 나온 차가운 공기는 서버실의 아래쪽부터 서서히 차오른다. 1분마다 컴퓨터 한 대의 높이만큼 방을 채운다. 이 회사의 서버 컴퓨터는 환경에 매우 민감하여 차가운 공기를 받아야만 동작하고 그렇지 못하면 장애를 일으킨다.

서버실의 컴퓨터 중 절반 이상이 켜지려면 몇 분이 필요할까?

{{< input >}}
입력
{{< /input >}}

정수 N이 주어진다. (1 ≤ N ≤ 1000)

N×N개의 각 칸에 컴퓨터가 몇 대 쌓여있는지가 입력된다. 한 칸에는 최대 10,000,000대까지 쌓여있을 수 있다.

```
5
1 4 0 2 1
0 0 5 6 3
8 4 7 2 0
7 1 0 5 3
4 5 7 9 1
```

{{< output >}}
출력
{{< /output >}}

몇 분이 지나야 전체 컴퓨터의 절반 이상이 장애를 일으키지 않고 동작할 수 있는지 출력한다.

```
3
```
{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
    'use strict';
    let rooms
    let totalPCCount
    let halfPCCount
    let maxPC
    let minPC
    const getPCCount = (min, pcCount) => {
      let onPC = 0
        if(min >= pcCount) {
          onPC = pcCount 
        } else {
          onPC = min
        }
      return onPC
    }

    const getTotalPCCount = (acc, cur) => {
        return acc + cur
    }

    const getMaxPCCount = (max, cur) => {
      return max >= cur ? max : cur
    }

    const getToTalOnPC = (mid, rooms) => {
        const afterMinRooms = rooms.map((pcCount) => getPCCount(mid, pcCount))
        const onPCs = afterMinRooms.reduce(getTotalPCCount)
        if (onPCs < halfPCCount) {
          minPC = ++mid
          if (minPC <= maxPC) {
            mid = Math.floor((minPC + maxPC) / 2)
            mid = getToTalOnPC(mid, rooms)
          } else {
            mid = --mid
          }
        } else if (onPCs > halfPCCount){
          maxPC = --mid
          if (minPC <= maxPC) {
            mid = Math.floor((minPC + maxPC) / 2)
            mid = getToTalOnPC(mid, rooms)
          } else {
            mid = ++mid
          }
        }
        return mid
    }

    const setRoomPC = (size) => {
      rooms = Array(size).fill().map(i=> Math.floor(Math.random()*10000000 + 1))
      totalPCCount = rooms.reduce(getTotalPCCount)
      halfPCCount = Math.ceil(totalPCCount/2)
      maxPC = rooms.reduce(getMaxPCCount)
      minPC = 1
    }

    const getResult = () => {
      let mid = Math.floor((minPC + maxPC) / 2)
      return getToTalOnPC(mid, rooms)
    }

    setRoomPC(1000)
    console.log(getResult())
{{< /highlight >}}
