---
title: "Count Number"
displayTitle: ""
date: 2020-11-20T09:47:15+09:00
isCJKLanguage: true
draft: false
categories: []
levels: [1]
description: "세 개의 자연수 A, B, C가 주어질 때 A×B×C를 계산한 결과에 0부터 9까지 각각의 숫자가 몇 번씩 쓰였는지를 구하는 프로그램을 작성하시오."
tags: ["string"]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "e0655861ae621f9cdcc5e83eb727f30c"
disqus_title: "Count Number"
disqus_url: "e0655861ae621f9cdcc5e83eb727f30c"
---

## 숫자의 개수

{{< problem >}}
문제
{{< /problem >}}

세 개의 자연수 A, B, C가 주어질 때 A×B×C를 계산한 결과에 0부터 9까지 각각의 숫자가 몇 번씩 쓰였는지를 구하는 프로그램을 작성하시오.

예를 들어 A = 150, B = 266, C = 427 이라면 

A × B × C = 150 × 266 × 427 = 17037300 이 되고, 

계산한 결과 17037300 에는 0이 3번, 1이 1번, 3이 2번, 7이 2번 쓰였다.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 A, 둘째 줄에 B, 셋째 줄에 C가 주어진다. A, B, C는 모두 100보다 같거나 크고, 1,000보다 작은 자연수이다.

{{< output >}}
출력
{{< /output >}}

첫째 줄에는 A×B×C의 결과에 0 이 몇 번 쓰였는지 출력한다. 마찬가지로 둘째 줄부터 열 번째 줄까지 A×B×C의 결과에 1부터 9까지의 숫자가 각각 몇 번 쓰였는지 차례로 한 줄에 하나씩 출력한다.


{{< example >}}
입출력 예제
{{< /example >}}

```js
// 예제 입력 1 
150
266
427
// 예제 출력 1 
3
1
0
2
0
0
0
2
0
0
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const input = require('fs')
  .readFileSync('/dev/stdin')
  .toString()
  .trim()
  .split(/\n+/)
  .map((i) => parseInt(i.toString().trim(), 10))
const [A, B, C] = input

const main = (multiply) => {
  const save = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    7: 0,
    8: 0,
    9: 0
  }
  multiply
    .toString()
    .split('')
    .forEach((number) => {
      if (save[number] === 0) {
        save[number] = 1
      } else {
        save[number] += 1
      }
    })
  for (const [key, value] of Object.entries(save).sort((a, b) => a[0] - b[0])) {
    console.log(value)
  }
}

main(A * B * C)
{{< /highlight >}}
