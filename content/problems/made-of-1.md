---
title: "Made of 1"
displayTitle: "1로 만들기"
date: 2020-10-30T16:50:08+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "정수 N이 주어졌을 때, 위와 같은 연산 세 개를 적절히 사용해서 1을 만들려고 한다. 연산을 사용하는 횟수의 최솟값을 출력하시오."
tags: [bfs]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "068a160476a00b04825da00bc67ebdac"
disqus_title: "Made of 1"
disqus_url: "068a160476a00b04825da00bc67ebdac"
---

## 1로 만들기

{{< problem >}}
문제
{{< /problem >}}

정수 X에 사용할 수 있는 연산은 다음과 같이 세 가지 이다.

X가 3으로 나누어 떨어지면, 3으로 나눈다.
X가 2로 나누어 떨어지면, 2로 나눈다.
1을 뺀다.
정수 N이 주어졌을 때, 위와 같은 연산 세 개를 적절히 사용해서 1을 만들려고 한다. 연산을 사용하는 횟수의 최솟값을 출력하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 1보다 크거나 같고, 10^6보다 작거나 같은 정수 N이 주어진다.

{{< output >}}
출력
{{< /output >}}

첫째 줄에 연산을 하는 횟수의 최솟값을 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
2
//출력1
1
//입력2
10
//출력2
3
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
const N = parseInt(require('fs').readFileSync('/dev/stdin').toString().trim(), 10)
const bfs = (N) => {
  let count = 0
  let queue = []
  const visited = new Set()
  queue.push(N)
  visited.add(N)
  while (queue.length) {
    const newQueue = []
    for (const value of queue) {
      if (value === 1) {
        console.log(count)
        return
      }
      if (value % 3 === 0 && !visited.has(value / 3)) {
        const temp = Math.floor(value / 3)
        newQueue.push(temp)
        visited.add(temp)
      }
      if (value % 2 === 0 && !visited.has(value / 2)) {
        const temp = Math.floor(value / 2)
        newQueue.push(temp)
        visited.add(temp)
      }
      {
        const temp = value - 1
        if (!visited.has(temp)) {
          newQueue.push(temp)
          visited.add(temp)
        }
      }
    }
    queue = newQueue
    count++
  }
}
bfs(N)
{{< /highlight >}}
