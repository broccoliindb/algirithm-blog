---
title: "Sugar Delivery"
displayTitle: "설탕 배달"
date: 2020-10-30T16:50:19+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "상근이가 설탕을 정확하게 N킬로그램 배달해야 할 때, 봉지 몇 개를 가져가면 되는지 그 수를 구하는 프로그램을 작성하시오."
tags: [math]
solutions: [completed]
authors: [broccoliindb]
weight: 0
disqus_identifier: "831a835d2ba3de83b3426630cbf597c9"
disqus_title: "Sugar Delivery"
disqus_url: "831a835d2ba3de83b3426630cbf597c9"
---

## 설탕 배달

{{< problem >}}
문제
{{< /problem >}}

상근이는 요즘 설탕공장에서 설탕을 배달하고 있다. 상근이는 지금 사탕가게에 설탕을 정확하게 N킬로그램을 배달해야 한다. 설탕공장에서 만드는 설탕은 봉지에 담겨져 있다. 봉지는 3킬로그램 봉지와 5킬로그램 봉지가 있다.

상근이는 귀찮기 때문에, 최대한 적은 봉지를 들고 가려고 한다. 예를 들어, 18킬로그램 설탕을 배달해야 할 때, 3킬로그램 봉지 6개를 가져가도 되지만, 5킬로그램 3개와 3킬로그램 1개를 배달하면, 더 적은 개수의 봉지를 배달할 수 있다.

상근이가 설탕을 정확하게 N킬로그램 배달해야 할 때, 봉지 몇 개를 가져가면 되는지 그 수를 구하는 프로그램을 작성하시오.

{{< input >}}
입력
{{< /input >}}

첫째 줄에 N이 주어진다. (3 ≤ N ≤ 5000)

{{< output >}}
출력
{{< /output >}}

상근이가 배달하는 봉지의 최소 개수를 출력한다. 만약, 정확하게 N킬로그램을 만들 수 없다면 -1을 출력한다.

{{< example >}}
입출력 예제
{{< /example >}}

```js
//입력1
18
//출력1
4
//입력2
4
//출력2
-1
//입력3
6
//출력3
2
//입력4
9
//출력4
3
//입력5
11
//출력5
3
```

{{< solution >}}
#. 풀이확인 JS
{{< /solution >}}

{{< highlight "js" "linenos=table,linenostart=1" >}}
let input = require('fs').readFileSync('/dev/stdin')
const N = parseInt(input.toString().trim(), 10)
const getResult = () => {
  let x = 0
  let y = 0
  y = Math.floor(N / 5)
  while (y >= 0) {
    if ((N - (5 * y)) % 3 === 0) {
      x = (N - (5 * y)) / 3
      console.log(x + y)
      break
    }
    y--
  }
  if (y < 0) {
    console.log(-1)
  }
}
getResult()
{{< /highlight >}}
