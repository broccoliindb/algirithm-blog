---
title: "Release"
displayTitle: ""
date: 2020-08-28T00:34:12+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: ""
tags: []
solutions: []
authors: []
weight: 5
disqus_identifier: "682e243cdd6f3146c9dd729276abfd7b"
disqus_title: "Release"
disqus_url: "682e243cdd6f3146c9dd729276abfd7b"
---

## Release History

|버전|작업내용|날짜|
|---|---|---|
|v1.0|사이트 첫번째 버전|2020-08-30|