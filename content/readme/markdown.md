---
title: "Markdown"
displayTitle: ""
date: 2020-08-28T14:24:32+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "마크다운의 간단한 사용법을 설명합니다."
tags: [markdown]
solutions: []
authors: []
weight: 4
disqus_identifier: "21f1d774bc67296f7656b45278cc0c73"
disqus_title: "Markdown"
disqus_url: "21f1d774bc67296f7656b45278cc0c73"
---

## 마크다운

마크다운은 익숙해지면 누구나 쉽게 html로 변환이 가능한 문자를 작성할 수 있다.

단지 아직 마크다운의 대한 표준이 없기 때문에 사용하는 곳에 따라 조금 효과가 다를 수 있다.

## Heading

|작성예제|작성표시화면|
|---|---|
|{{< highlight "js" "linenos=table" >}}

# 문서전체 헤딩
## 하위주제 헤딩
### 하위주제 헤딩
#### 하위주제 헤딩
##### 하위주제 헤딩
###### 하위주제 헤딩

{{< /highlight >}}| ![headingImage](/readme/images/heading.png) |

## BlockQuote

```
> 보통 인용문을 사용할 때 쓴다.
```
> 보통 인용문을 사용할 때 쓴다.

## 목록

```
- 목록1
- 목록2
- 목록3
  - 세부목록1
    - 세부세부
```

- 목록1
- 목록2
- 목록3
  - 세부목록1
    - 세부세부

## 코드

일반적인 markdown으로는 

\`\`\`
\`\`\`

을 사용해서 코드를 작성하고 여기서도 그렇게 사용해도 괜찮습니다. 

하지만 `코드의 라인`이나 `복사기능`을 ***사용할수는 없습니다***. 따라서 라인넘버나 복사기능 사용할 때는 아래와 같이 사용하세요.

아래 코드는 상단에 제공한 [snippet](/readme/quick-start/#1-snippet-추가하기) 을 사용하면 간단히 적용가능합니다.

{{< highlight "js" "linenos=table,hl_lines=1,linenostart=1" >}}
{{</* highlight "js" "linenos=table,hl_lines=1,linenostart=1" >}}

{{< /highlight */>}}
{{< /highlight >}}

> `linenos=table` 속성이 있어야 복사기능과 라인넘버가 생성이 됩니다.

> `linenos=table,hl_lines=1` **hi_lines** 속성이 추가되면 해당 라인의 음영이 생성됩니다. **1-10** 혹은 **1,2** 과 같이 사용가능합니다.

> `linenos=table,hl_lines=1,linenostart=1` **linenostart** 속성이 추가되면 해당 라인수가 첫번째 라인넘버가 됩니다.

## 수평선

\<hr\>
은 dash(`-`) 3개로 표현가능합니다.

---

## 링크

```
[표현될 이름](url)
```

## font-style

### 강조

```
**강조**
```
**강조**

### 이탤릭

```
*이탤릭*
```
*이탤릭*

### cancelLine

```
~~cancelLine~~
```
~~cancelLine~~

## 이미지

이미지를 담는 장소는 `해당 md파일의 폴더 내부 > images` 입니다.

```
[!alt명칭]("/해당md파일의폴더내부/images/이미지명.png")
//ex) problems 폴더
[!treeImage]("/problems/images/tree.png")
```