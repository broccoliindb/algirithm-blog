---
title: "Front Matter"
displayTitle: ""
date: 2020-08-28T00:33:08+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "휴고 사이트 컨텐츠의 메타데이터: Front Matter"
tags: []
solutions: []
authors: []
weight: 2
disqus_identifier: "ab032618867bd830164da8b6b8b6a683"
disqus_title: "Front Matter"
disqus_url: "ab032618867bd830164da8b6b8b6a683"
---

## Front Matter

Front Matter는 hugo site에서 각 컨텐츠가 가지는 meta 정보입니다.

디폴트설정은 소스내 archetypes > default.md에 있습니다.

아래 프론트매터는 hugo new로 컨텐츠를 추가했을 때 생성되는 정보 중 예를 든 것입니다.

작성방법은 YAML, TOML, JSON 다 가능합니다. 이 사이트는 YAML로 되어있습니다.

{{< highlight "js" "linenos=table,hl_lines=3 6-12,linenostart=1" >}}

---
title: "Front Matter"
displayTitle: ""
date: 2020-08-28T00:33:08+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "휴고 사이트 컨텐츠의 메타데이터: Front Matter"
tags: []
solutions: []
authors: []
weight: 2
disqus_identifier: "ab032618867bd830164da8b6b8b6a683"
disqus_title: "Front Matter"
disqus_url: "ab032618867bd830164da8b6b8b6a683"
---

{{< /highlight >}}

- `음영이 칠해진부분만` 파일 생성후 필요시 작성하시면 되고 필요없으시면 작성 안하시고 디폴트로 두셔도 됩니다.
- `[]` 형태로 되어있는 부분은 분류필터를 적용하기 위해 만든 부분이기에 여러키워드를 넣을 수도 있습니다. 
- 단 levels, solutions의 경우 키워드를 따로 지정했으니 해당 지정키워드만 넣어주시기 바랍니다.

### title

파일이 생성될때 자동으로 생성되는 이름입니다.

### displayTitle

브라우저상으로 보이는 이름입니다. 없으면 title이 대신 출력됩니다.

### date

파일이 생성될때 자동으로 현재시간이 생성이 됩니다.

### isCJKLanguage

중국어, 한국어, 일본어는 문자 스트링 토큰 타입이 영어와 다르기 때문에 설정해 주었습니다.

### draft

디폴트는 true입니다. true로 했을 때는 운영환경에서 노출되지 않는 화면입니다. 작성이 마무리 되지 않았을 경우에는 `true`로 그대로 두고 완성이 되었다면 `false`로 변경하시면 됩니다.

### categories

분류별리스트에 필터링 되는 키워드입니다. `["a","b","c"]`와 같이 배열 형태로 여러가지 키워드를 넣을 수 있고 이것을 토대로 메뉴에 있는 `분류별리스트`가 필터링 됩니다. 

**해당 속성의 `목적`은 알고리즘문제나 이론을 카테고리로써 분류 하기 위한 속성입니다.**

### levels

> 키워드 : level1, level2, level3, level4, level5

분류별리스트와 마찬가지로 `단계별리스트`에서 동일하게 필터역할을 합니다. 이것의 경우는 위의 5개의 키워드로 등록해주셔야 합니다. `level1: easy ~level5: hard` 입니다.
["level1"] 이와 같이 등록해주시고 마찬가지로 중복등록도 가능합니다.

### description

해당 컨텐츠의 설명부분입니다.

### tags

태그도 배열형태로 필터링이 됩니다. 필터링시 메뉴의 `태그`에서 확인 가능하며 키워드는 따로 없습니다.

태그를 카테고리와 비슷하게 볼 수도 있겠으나... 

`태그의 목적`은 **카테고리로 구분지어진 문서들 내부에 특정 키워드를 제공하여 좀 더 작은 규모의 *분류필터링* 을 위한 목적으로 넣었습니다.** 

### solutions

> 키워드 : completed, uncompleted

메뉴의 `해결 / 미해결`에서 동일하게 필터역할을 합니다. 이것의 경우는 위의 2개의 키워드로 등록해주셔야 합니다.

### authors

메뉴의 `작성자`에서 확인 가능하며 `git config user.name`에 해당하는 것을 넣어주시면 됩니다.

### weight

작성된 컨텐츠의 순서를 결정해주는 속성인데 이 사이트는 git의 마지막 수정날짜를 역순으로 조회하도록 해서 딱히 신경쓰지 않으셔도 됩니다.

매뉴얼하게 필요한 경우 작성하기 위해서 포함시켜놓은 속성입니다.