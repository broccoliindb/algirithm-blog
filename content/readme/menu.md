---
title: "Menu"
displayTitle: ""
date: 2020-08-28T00:33:27+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "메뉴 설명"
tags: [menu]
solutions: []
authors: []
weight: 3
disqus_identifier: "8b85bf172f09360c2ec5c48b18e0a878"
disqus_title: "Menu"
disqus_url: "8b85bf172f09360c2ec5c48b18e0a878"
---

## Menu

메뉴는 `현재(v1.0)` 아래와 같은 구조로 되어있고 변경이 될 수 있습니다.

- 알고리즘 : 알고리즘 이론정리를 위한 메뉴입니다.
- 모든문제 : 알고리즘 스터디를 위한 모든 *문제* 는 **`이곳`**에 작성해야합니다.
- 최근업데이트 : 현 사이트에 작성된 모든 문서 중 최신 `10건`만 시간 역순으로 위치하고 있습니다.
- 단계별리스트 : `levels` 라는 항목에 정보를 입력하면 자동으로 분류되어 해당 메뉴에서 조회가능 합니다.
  > 키워드 : *level1* *level2* *level3* *level4* *level5* 입니다.
- 분류별리스트 : `categories` 라는 항목에 정보를 입력하면 자동으로 분류되어 해당 메뉴에서 조회가능 합니다.
  > 키워드 ❌
- 해결 / 미해결 : `solutions` 라는 항목에 정보를 입력하면 자동으로 분류되어 해당 메뉴에서 조회가능 합니다.
  > 키워드 : *completed* *uncompleted*
- 작성자 : `authors` 라는 항목의 본인의 *git config user.name* 에 해당하는 정보를 입력하면 자동으로 분류되어 해당 메뉴에서 조회가능 합니다.
- 태그 : `tags` 라는 항목의 원하는 태그정보를 입력하면 자동으로 분류되어 해당 메뉴에서 조회가능 합니다.
