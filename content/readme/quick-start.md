---
title: "Quick Start"
displayTitle: ""
date: 2020-08-27T21:34:26+09:00
isCJKLanguage: true
draft: false
categories: []
levels: []
description: "사이트의 문서 작성 Quick Start 매뉴얼"
tags: [quick start]
solutions: []
authors: []
weight: 1
disqus_identifier: "8c70151f2548c468978a5e2aa56ade99"
disqus_title: "Quick Start"
disqus_url: "8c70151f2548c468978a5e2aa56ade99"
resources:
- title: snippets
  src: /downloads/markdown.json 
  download: true
---
## Requirements

> ❗window사용자는 환경변수로 인해 인식이 안될 수 있으니 환경변수 설정을 잘 확인한다.

사이트 컨텐츠 추가 및 로컬서버를 실행하기 위해서는 아래의 사항들이 설치가 되어있어야 한다.


- [Git Installation](/readme/quick-start/#1-git-installation)
- [node / npm Installation](/readme/quick-start/#2-node--npm-installation)
- [Hugo Installation](/readme/quick-start/#3-hugo-installation)

### 1. Git Installation

[git 설치 링크](https://git-scm.com/book/ko/v2/%EC%8B%9C%EC%9E%91%ED%95%98%EA%B8%B0-Git-%EC%84%A4%EC%B9%98)

설치후 버전확인이 된다면 설치가 잘 된것임. 

```js
git version

//git version 2.25.1 버전정보는 다를 수 있음.
```

### 2. node / npm Installation

> ❗노드설치시에는 반드시 `LTS버전`으로 설치하고 Linux나 Mac 사용자의 경우에는 터미널을 통해 `nvm`으로 설치하기를 권장

node 설치하면 npm은 자동으로 같이 설치됨

[노드설치링크](https://nodejs.org/ko/)

```js
node --version //v12.18.3 버전정보는 다를 수 있음.
npm --version //6.14.6 버전정보는 다를 수 있음.
```

### 3. Hugo Installation

hugo 버전은 반드시 0.74.3이상이어야함.

> Why ❓이전과 이후 버전의 분류키워드명칭이 변경되어있음. 
해당 사이트는 최신버전의 분류키워드를 통해 해당 사이트를 만들었음.

#### Windows : choco package manager설치

파워셀 터미널로 아래 명령어를 실행한다. 반드시 ❗`관리자`로 실행함!!!

```js
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

```js
//버전확인되면 설치됨
choco
```

```js
choco install hugo -confirm
```

[choco 참고링크](https://chocolatey.org/packages/hugo#install) 

#### Ubuntu

```js
sudo apt-get update
sudo apt-get install hugo
hugo version
```

#### Mac

```js
brew install hugo
hugo version
```
## localhost 시작하기

### 1. 다운받기

```js
//ssh
git clone git@bitbucket.org:broccoliindb/algirithm-blog.git

//https
git clone https://yongbumchoi@bitbucket.org/broccoliindb/algirithm-blog.git
```

### 2. localhost 띄우기

```js
npm install

//localhost:1313
npm run dev
```

## 컨텐츠 추가하기

### 1. snippet 추가하기

본격적인 컨텐츠 추가에 앞서 몇가지 snippets을 등록해두면 좋다.
여럿이 공통으로 사용하는 사이트이기 때문에 보편적인 컨텐츠 틀을 유지하기 위해서 몇가지 shortcode를 만들었고 사용의 용이를 위해서 해당 shortcodes를 snippets으로 등록해 두자.

> **Shortcode ❓** Hugo 내의 markdown 페이지에서 사용할 수 있는 custom HTML:

각자사용하는 Editor혹은 IDE에 snippets을 추가하는 기능이 있을 것이다.
본인이 사용하는 visual studio code를 예를 들어서 설명하면 아래와 같음.

#### 세팅위치

 `F1 단축키 > Configure User Snippets > markdown.json` 

#### 다운로드

 {{< downloads "snippets">}}

### 2. "모든 문제" 컨텐츠 추가 하기

#### 문제 컨텐츠 파일 생성 하기

문서명칭은 **kebab-case**로 작성하세요.

```js
//on project path (ex: current location : workspace/algorithm-blog)

hugo new problems/new-added-name.md
```

#### 문제 컨텐츠 작성하기

아래의 작성예제를 토대로 

`문제` 👉🏻 `입력` 👉🏻 `출력` 👉🏻 `풀이확인` 👉🏻 `해답코드` 

순서로 작성을 하시면 됩니다.

##### 1) 문제

|작성예제|작성표시화면|
|---|---|
|{{< highlight "js" "linenos=table" >}}

{{</*problem*/>}}
문제
{{</*/problem*/>}}

문제내용적기
{{< /highlight >}}|{{<problem>}}
문제
{{</problem>}} 문제내용적기|

##### 2) 입력

|작성예제|작성표시화면|
|---|---|
|{{< highlight "js" "linenos=table" >}}

{{</*input*/>}}
입력
{{</*/input*/>}}

입력내용적기
{{< /highlight >}}|{{<input>}}
입력
{{</input>}} 입력내용적기|

##### 3) 출력

|작성예제|작성표시화면|
|---|---|
|{{< highlight "js" "linenos=table" >}}

{{</*output*/>}}
출력
{{</*/output*/>}}

출력내용적기
{{< /highlight >}}|{{<output>}}
출력
{{</output>}} 출력내용적기|

##### 4) 풀이확인

|작성예제|작성표시화면|
|---|---|
|{{< highlight "js" "linenos=table" >}}

{{</*solution*/>}}
풀이확인
{{</*/solution*/>}}

{{< /highlight >}}|{{<solution>}}
풀이확인
{{</solution>}}|

##### 5) 해답 코드

|작성예제|작성표시화면|
|---|---|
|{{< highlight "js" "linenos=table" >}}

{{</*highlight "js" "linenos=table"*/>}}
...코드
{{</*/highlight*/>}}

{{< /highlight >}}|{{< highlight "js" "linenos=table" >}}
...코드
{{< /highlight >}}|

### 3. "알고리즘" 컨텐츠 추가하기

#### 알고리즘 컨텐츠 파일 생성 하기

문서명칭은 **kebab-case**로 작성하세요.

```js
//on project path (ex: current location : workspace/algorithm-blog)

hugo new algorithms/new-added-name.md
```

#### 알고리즘 컨텐츠 작성하기

컨텐츠 작성은 일반적인 markdown 작성과 동일하게 생각하면 됩니다.

##### 1) 타이틀

|작성예제|작성표시화면|
|---|---|
|{{< highlight "js" "linenos=table" >}}

# 문서전체 헤딩
## 하위주제 헤딩
### 하위주제 헤딩
#### 하위주제 헤딩
##### 하위주제 헤딩
###### 하위주제 헤딩

{{< /highlight >}}| ![headingImage](/readme/images/heading.png) |

##### 2) 문단

그냥 이렇게 지금처럼 적으시면 됩니다.
이렇게 붙여서쓰면 하나의 `<p>`안에 글이 작성이되고

이렇게 new line(⤶⤶)을 하거나 붙여서 작성하더라도 (`space` `space`⤶) 하면 `<p>`가 새로 생성됩니다.

##### 3) 코드

일반적인 markdown으로는 

\`\`\`
\`\`\`

을 사용해서 코드를 작성하고 여기서도 그렇게 사용해도 괜찮습니다. 

하지만 `코드의 라인`이나 `복사기능`을 ***사용할수는 없습니다***.

아래 코드는 상단에 제공한 [snippet](/readme/quick-start/#1-snippet-추가하기) 을 사용하면 간단히 적용가능합니다.

{{< highlight "js" "linenos=table,hl_lines=1,linenostart=1" >}}
{{</* highlight "js" "linenos=table,hl_lines=1,linenostart=1" >}}

{{< /highlight */>}}
{{< /highlight >}}

> `linenos=table` 속성이 있어야 복사기능과 라인넘버가 생성이 됩니다.

> `linenos=table,hl_lines=1` **hi_lines** 속성이 추가되면 해당 라인의 음영이 생성됩니다. **1-10** 혹은 **1,2** 과 같이 사용가능합니다.

> `linenos=table,hl_lines=1,linenostart=1` **linenostart** 속성이 추가되면 해당 라인수가 첫번째 라인넘버가 됩니다.

##### 4) 이미지

일반적인 마크다운 작성법과 동일합니다.

이미지를 담는 장소는 `해당 md파일의 폴더 내부 > images` 입니다.

```js
![alt명칭]("/해당md파일의폴더내부/images/이미지명.png")
//ex) problems 폴더
![treeImage]("/problems/images/tree.png")
```

