import gulp from 'gulp'
import shell from 'gulp-shell'
import htmlmin from 'gulp-htmlmin'
import cleanCSS from 'gulp-clean-css'
import uglifyify from 'uglifyify'
import bro from 'gulp-bro'
import babelify from 'babelify'

gulp.task('hugo-build', shell.task(['hugo']))

gulp.task('minify-html', () => {
  return gulp.src('public/**/*.html')
    .pipe(htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      removeComments: true,
      useShortDoctype: true,
    }))
    .pipe(gulp.dest('./public'))
})

gulp.task('minify-xml', () => {
  return gulp.src('public/**/*.xml')
    .pipe(htmlmin({
      collapseWhitespace: true
    }))
    .pipe(gulp.dest('./public'))
})

gulp.task('minify-css', () => {
  return gulp.src('public/css/styles.css')
    .pipe(cleanCSS())
    .pipe(gulp.dest('./public/css'))
})

gulp.task('minify-js', () => {
  return gulp.src([
    'public/**/*.js'
  ])
    .pipe(bro({
      transform: [
        babelify.configure({
          presets: [["@babel/preset-env", { targets: { chrome: "55" } }]],
        }),
        ['uglifyify', { global: true }]
      ]
    }))
    .pipe(gulp.dest('./public'))
})

export const prod = gulp.series(["hugo-build", 'minify-html', 'minify-xml', 'minify-css', 'minify-js'])


