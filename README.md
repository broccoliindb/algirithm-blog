# grab-Algorithm

## **알고리즘 정리 및 해결된 문제 정리 블로그**

> 알고리즘 스터디원들과 같이 문제해결과 응답 문의 질의 등을 함께 하기 위해 만든 사이트

https://grab-algorithm.netlify.app/


![coverimage](./cover.png)

## 사용기술

- hugo : 정적사이트 생성 프레임워크
- css
- javascript
- flexSearch.js : 검색서비스
- disqus : 댓글서비스

## 빌드

- gulp를 이용해서 자동 빌드 및 배포 파이프라인 구현
- netlify를 이용한 배포

```
git push origin master
```
