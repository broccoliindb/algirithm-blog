const solutions = document.querySelectorAll('.solution')

const solutionClickedHandler = (evt) => {
  const target = evt.target
  if (!target.closest('.solution')) return
  if (target.nextElementSibling) {
    const highlight = target.nextElementSibling.closest('.highlight')
    highlight.classList.toggle('hidden')
  }
}

Array.from(solutions).forEach(item => {
  if(item && item.nextElementSibling) {
    const highlight = item.nextElementSibling.closest('.highlight')
    if (highlight) {
      item.addEventListener('click', solutionClickedHandler)
      if (!highlight.classList.contains('hidden')) {
        highlight.classList.add('hidden')
      }
    }
  }
})