const body = document.body
const container = document.querySelector('.container')
if (container) {
  const asideNav = container.previousElementSibling
  const navOffSide = asideNav.querySelector('.nav__offSide')
  const bar = container.querySelector('.bar')
  if (bar) {
    bar.addEventListener('click', function () {
      asideNav.classList.toggle('showMenu')
      container.classList.toggle('showMenu')
    })
    navOffSide.addEventListener('click', function (evt) {
      const target = evt.target
      if (target.closest('.bar')) return

      asideNav.classList.remove('showMenu')
      container.classList.remove('showMenu')
    })
  }
}