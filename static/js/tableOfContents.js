const topToBottomHandler = () => {
  const topToBottom = document.querySelector('.topToBottom')
  if ( topToBottom) {
    const top = topToBottom.querySelector('.top')
    const bottom = topToBottom.querySelector('.bottom')
    const moveTop = () => {
      window.scrollTo(0,0)
    }
    const moveBottom = () => {
      window.scrollTo(0, document.body.scrollHeight)

    }
    top.addEventListener('click', moveTop)
    bottom.addEventListener('click', moveBottom)
  }
}

const tocHandler = () => {
  const miniTableOfContents = document.querySelector('.tableOfContents .miniTableOfContents')
  if (miniTableOfContents) {
    const toc = miniTableOfContents.previousElementSibling
    if (toc && toc.hasChildNodes()) {
      miniTableOfContents.style.display = "block"
      const showTOCHandler = (evt) => {
        if (toc && toc.hasChildNodes()) {
          toc.classList.toggle('show')
        }
      }
      miniTableOfContents.addEventListener('click', showTOCHandler)
    } else {
      miniTableOfContents.style.display = "none"
    }
  }
}
window.addEventListener('load', tocHandler)
window.addEventListener('load', topToBottomHandler)