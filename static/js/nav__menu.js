const nav = document.querySelector('.nav')
if (nav) {
  const navMenu = nav.querySelector('.menu')
  const menuItems = [...navMenu.querySelectorAll('.menu__item')]
  const menuClickedHandler = (evt) => {
    const target = evt.target
    const menuItem = target.closest('.menu__item')
    if (!menuItem && !menuItems) return
    const oldIndex = menuItems.findIndex(item => item.classList.contains('selected'))
    const newIndex = menuItems.indexOf(menuItem)
    let showSubMenus = true
    if (oldIndex === newIndex) {
      showSubMenus = false
    }
    menuItems.forEach((item, index) => {
      item.classList.remove('selected')
    })
    if (showSubMenus) {
      menuItem.classList.add('selected')
      showSubMenus = true
    }
  }

  navMenu.addEventListener('click', menuClickedHandler)
}

